source('/home/admin/CODE/JasonGraphs/KH-003X/[715_KH-003] Data_extract.R')
require('compiler')
enableJIT(3)

rm(list=ls(all =TRUE))
library(ggplot2)
library(zoo)
pathWrite <- "/home/admin/Jason/cec intern/results/KH-003X/"
result <- read.csv("/home/admin/Jason/cec intern/results/715/[715]_summary.csv")
ma <- function(x,y,del)
{
  v1 <- zoo(x,as.Date(y))
  v2 <- rollapplyr(v1, list(-(del:1)), mean, fill = NA, na.rm = TRUE)
  return(v2)
}
station <- "[KH-003]"
rownames(result) <- NULL
result <- data.frame(result)
result <- result[-length(result[,1]),]
result <- result[,(-1)]
colnames(result) <- c("date","da","pts","uptime","wuptime","pr")
result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- as.numeric(paste(result[,5]))
result[,6] <- as.numeric(paste(result[,6]))
date <- result[,1]

ma1 <- ma(result[,6][result[,6] < 101], date, 30)
result$ambpr1.av=coredata(ma1)


dagraph <- ggplot(result, aes(x=date,y=da))+ylab("Data Availability [%]")
daFinalGraph<- dagraph + geom_line(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_y_continuous(breaks=seq(0, 105, 10))+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  ggtitle(paste(station," Data Availability"), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)))+
  annotate("text",label = paste0("Average data availability = ", round(mean(result[,2]),1),"%"),size=3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 52)+
  annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)   "),size = 3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 57)+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

daFinalGraph
ggsave(daFinalGraph,filename = paste0(pathWrite,station,"715_DA_LC.pdf"), width =7.92, height =5)

uptimeGraph <- ggplot(result, aes(x=date,y=uptime))+ylab("Uptime [%]")
uptimeFinalGraph<- uptimeGraph + geom_line(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  scale_y_continuous(breaks=seq(0, 115, 10))+
  coord_cartesian(ylim=c(0,115))+
  ggtitle(paste("Uptime for ",station), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)))+
  annotate("text",label = paste0("Uptime Percentage = ", format(round(mean(result[,4][!is.na(result[,4])]),1),nsmall=1),"%"),size=3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 112)+
  annotate("text",label = paste0("System Lifetime = ", length(date)," days (",round(length(date)/365,1)," years)      "),size = 3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 107)+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

uptimeFinalGraph
ggsave(uptimeFinalGraph,filename = paste0(pathWrite,station,"_PAC_LC.pdf"), width =7.92, height =5)


wuptimeGraph <- ggplot(result, aes(x=date,y=wuptime))+ylab("Weighted uptime [%]")
wuptimeFinalGraph<- wuptimeGraph + geom_line(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  scale_y_continuous(breaks=seq(0, 115, 10))+
  coord_cartesian(ylim=c(0,115))+
  ggtitle(paste("Irradiance - weighted uptime for ",station), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)))+
  annotate("text",label = paste0("Irradiance - Weighted uptime percentage, Pac > 0.1 kW = ", format(round(mean(result[,5][!is.na(result[,5])]),1),nsmall=1),"%"),size=3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 112)+
  annotate("text",label = paste0("System Lifetime = ", length(date)," days (",round(length(date)/365,1)," years)                     "),size = 3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 107)+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

wuptimeFinalGraph
ggsave(wuptimeFinalGraph,filename = paste0(pathWrite,station,"715_WPAC_LC.pdf"), width =7.92, height = 5)

lastDate = as.character(result[nrow(result),1])
lastPR = as.character(result[nrow(result),6])
prGraph <- ggplot(data = result, aes(x=date,y=pr)) 
prFinalGraph<- prGraph + geom_point(size=0.5) +
  theme_bw() + ylab("Performance Ratio [%]") +
  expand_limits(x=date[1], y=0)+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  scale_y_continuous(breaks=seq(0, 100, 10))+
  coord_cartesian(ylim=c(0,100))+
	annotate(geom="text",x = as.Date(date[round(0.75*length(date))]),y=95,label=paste("PR:",lastPR)) +
  ggtitle(paste0("PR for ",station), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  geom_line(data = result, aes(x=date, y=ambpr1.av), color = "red", size = 1.2) +
  theme(axis.title.y = element_text(size = 11, face = "bold", margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)))+
  geom_hline(yintercept=mean(result[,6][result[,6]<100&(!is.na(result[,6]))]),size=0.3)+
  annotate("text", label = paste0("Average PR = ", round(mean(result[,6][result[,6]<100&(!is.na(result[,6]))]),1),"%"), size = 3.6, 
           x = as.Date(date[round(0.518*length(date))]), y=91 , color="black")+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

prFinalGraph

ggsave(prFinalGraph,filename = paste0(pathWrite,station,"_PR_LC.pdf"),width =7.92, height =5)

#jpeg(filename = "/Users/liuchenxi/Desktop/715summary.png",width = 1275, height = 799,pointsize =12,quality = 10000)
#ggplot2.multiplot(daFinalGraph,wuptimeFinalGraph,uptimeFinalGraph,prFinalGraph, cols=2)
#dev.off()
