rm(list=ls(all =TRUE))


### change the pathRead
pathRead <- 
#######################

setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2016-09-17 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2016-09-17 17:00:00"), format="%H:%M:%S")

### change the station name
nameofStation <- 
##############################################


col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] = 0
index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(paste(temp[1,1]),1,10)
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability
  pts <- length(temp[,1])
  col3[index] <- pts
  
  #gsi
  #change the index of gsi
  gsi <- as.numeric(temp[,3])
  ########################
  
  col4[index] <- sum(gsi)/60000
  
  #hamb&tamb

  #change the index of ambient temperature
  tamb <- mean(as.numeric(temp[,4]))
  ########################################
  col5[index] <- tamb
  
  #change the index of ambient humidity
  hamb <- mean(as.numeric(temp[,5]))
  col6[index] <- hamb
  #########################################
  
  index <- index + 1
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]
col2 <- col3/max(col3)*100
col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA

