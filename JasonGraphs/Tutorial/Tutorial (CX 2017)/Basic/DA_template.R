rm(list=ls(all =TRUE))
library(ggplot2)

#working directory need to be changed
#setwd("/Users/liuchenxi/Dropbox/Gen 1 Data/[IN-001]/2016/")
filelist <- dir(pattern = ".txt", recursive= TRUE)
result <- NULL

#ideal total number of entries: 288 if entry recorded per 5 min, 1440 per 1 min.
total <- 288

#export data
for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(temp[1,1],1,10)
  pts <- length(temp[,1])
  d.a <- round(as.numeric(pts)/as.numeric(total)*100,1)
  data[1] <- date
  data[2] <- d.a
  data[3] <- pts
  result <- rbind(result,data)
}

#write result to txt file
result <- result[-length(result[,1]),]
colnames(result) <- c("date","D.A","No.of.Pts")
rownames(result) <- NULL
result <- data.frame(result)
#directory need to be changed
#write.table(result,file = "/Users/liuchenxi/Documents/CleanTech/result/IN-001/IN-001_DA_LC.txt",
#            row.names = FALSE)

#change the type of the data entry contained in the result list
result[,2] <- as.numeric(paste(result[,2]))
result[,1] <- as.Date(result[,1], origin = result[,1][1])

date = result[,1]

#graph plot

#setting x & y, and xlab & ylab. normally xlab is ommitted
graph <- ggplot(result, aes(x=date,y=D.A))+ylab("Data Availability [%]")

#size of the line
final_graph<- graph + geom_line(size=0.5)+
  #background color setting
  theme_bw()+
  #set the origin
  expand_limits(x=date[1],y=0)+
  #set the scale of y lab
  scale_y_continuous(breaks=seq(0, 100, 10))+
  #set the scale of x lab
  scale_x_date(date_breaks = "1 month",date_labels = "%b")+
  #write title for the graph
  ggtitle(paste("IN-001 Data Availability","\n",date[1]," to ",date[length(date)]))+
  #adjust the content of x&y lab and title: font size, font face, margin
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,margin=margin(0,0,7,0)))+
  #adding annotation: average & lifetime
  #remark: adding a few naumber of space is for alignment
  annotate("text",label = paste0("Average data availability = ", round(mean(result[,2]),1),"%"),size=4,
           x = as.Date(date[round(0.518*length(date))]), y= 25)+
  annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)   "),size = 4,
           x = as.Date(date[round(0.518*length(date))]), y= 20)+
  #adjust the margin of the plot
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

#showing the graph
final_graph

#directory need to be changed
#ggsave(final_graph,filename = "/Users/liuchenxi/Documents/CleanTech/result/IN-001/IN-001_DA_LC.pdf")
