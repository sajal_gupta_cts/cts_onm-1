rm(list = ls())

path = "/home/admin/Dropbox/Gen 1 Data/[PH-001]"

years = dir(path)
for(x in 1 : length(years))
{
	pathyears = paste(path,years[x],sep="/")
	mnths = dir(pathyears)
	for(y in 1 : length(mnths))
	{
		pathdays = paste(pathyears,mnths[y],sep="/")
		days = dir(pathdays)
		for(z in 1 : length(days))
		{
			print(days[z])
			pathac = paste(pathdays,days[z],sep="/")
			dataread = read.table(pathac,header = T,sep = "\t")
			oldcolnames = colnames(dataread)
			dataread[,(ncol(dataread)+1)] = rep(NA,nrow(dataread))
			colnames(dataread) = c(oldcolnames,"Tmod")
			write.table(dataread,file = pathac,row.names = F,col.names = T,sep ="\t",
			append = F)
		}
	}
}

