source('/home/admin/CODE/common/aggregate.R')

METERACNAMESAGG = c("WMS","DC","GH","MC","NC","SS","Inverter_1","Inverter_2","Inverter_3","Inverter_4","Inverter_5","Inverter_6","Inverter_7","Inverter_8","Inverter_9","Inverter_10",
	          "Inverter_11","Inverter_12","Inverter_13","Inverter_14","Inverter_15","Inverter_16","Inverter_17","Inverter_18","Inverter_19","Inverter_20",
	          "Inverter_21","Inverter_22","Inverter_23","Inverter_24","Inverter_25","Inverter_26","Inverter_27","Inverter_28","Inverter_29","Inverter_30","Inverter_31","Inverter_32",
			  "Inverter_33","Inverter_34","Inverter_35")

registerMeterList("IN-026C",METERACNAMESAGG)
for( x in 1 : length(METERACNAMESAGG))
{
		aggNameTemplate = getNameTemplate()
		aggColTemplate = getColumnTemplate()
		{
		if(grepl("WMS",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = NA #column for LastRead
			aggColTemplate[4] = NA #column for LastTime
			aggColTemplate[5] = NA #column for Eac-1
			aggColTemplate[6] = NA #column for Eac-2
			aggColTemplate[7] = NA #column for Yld-1
			aggColTemplate[8] = NA #column for Yld-2
			aggColTemplate[9] = NA #column for PR-1
			aggColTemplate[10] = NA #column for PR-2
			aggColTemplate[11] = 3 #column for Irr
			aggColTemplate[12] = "Self" # IrrSrc Value
			aggColTemplate[13] = 4 #column for Tamb
			aggColTemplate[14] = 5 #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
		}
		else if(grepl("DC",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 9 #column for LastRead
			aggColTemplate[4] = 10 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 5 #column for Yld-1
			aggColTemplate[8] = 6 #column for Yld-2
			aggColTemplate[9] = 7 #column for PR-1
			aggColTemplate[10] = 8 #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = "IN-026C-WMS" # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			aggColTemplate[16] = NA #column for IA
			aggColTemplate[17] = 11 #column for GA
			aggColTemplate[18] = 12 #column for PA
		}
		else if(grepl("GH",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 9 #column for LastRead
			aggColTemplate[4] = 10 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 5 #column for Yld-1
			aggColTemplate[8] = 6 #column for Yld-2
			aggColTemplate[9] = 7 #column for PR-1
			aggColTemplate[10] = 8 #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = "IN-026C-WMS" # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			aggColTemplate[16] = NA #column for IA
			aggColTemplate[17] = 11 #column for GA
			aggColTemplate[18] = 12 #column for PA
		}
		else if(grepl("MC",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 9 #column for LastRead
			aggColTemplate[4] = 10 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 5 #column for Yld-1
			aggColTemplate[8] = 6 #column for Yld-2
			aggColTemplate[9] = 7 #column for PR-1
			aggColTemplate[10] = 8 #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = "IN-026C-WMS" # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			aggColTemplate[16] = NA #column for IA
			aggColTemplate[17] = 11 #column for GA
			aggColTemplate[18] = 12 #column for PA
		}
		else if(grepl("NC",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 9 #column for LastRead
			aggColTemplate[4] = 10 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 5 #column for Yld-1
			aggColTemplate[8] = 6 #column for Yld-2
			aggColTemplate[9] = 7 #column for PR-1
			aggColTemplate[10] = 8 #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = "IN-026C-WMS" # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			aggColTemplate[16] = NA #column for IA
			aggColTemplate[17] = 11 #column for GA
			aggColTemplate[18] = 12 #column for PA
		}
		else if(grepl("SS",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 9 #column for LastRead
			aggColTemplate[4] = 10 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 5 #column for Yld-1
			aggColTemplate[8] = 6 #column for Yld-2
			aggColTemplate[9] = 7 #column for PR-1
			aggColTemplate[10] = 8 #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = "IN-026C-WMS" # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			aggColTemplate[16] = NA #column for IA
			aggColTemplate[17] = 11 #column for GA
			aggColTemplate[18] = 12 #column for PA
		}
		else if(grepl("Inverter",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 8 #column for LastRead
			aggColTemplate[4] = 9 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 6 #column for Yld-1
			aggColTemplate[8] = 7 #column for Yld-2
			aggColTemplate[9] = NA #column for PR-1
			aggColTemplate[10] = NA #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = NA # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			aggColTemplate[16] = 10 #column for IA
		}
		}
		registerColumnList("IN-026C",METERACNAMESAGG[x],aggNameTemplate,aggColTemplate)
}
