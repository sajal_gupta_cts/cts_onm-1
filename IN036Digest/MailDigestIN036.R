errHandle = file('/home/admin/Logs/LogsIN036Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

system('rm -R "/home/admin/Dropbox/Second Gen/[IN-036S]"')
rm(list = ls())
source('/home/admin/CODE/IN036Digest/runHistoryIN036.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
require('mailR')
require('tidyr')
print('History done')
source('/home/admin/CODE/IN036Digest/3rdGenDataIN036.R')
print('3rd gen data called')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/IN036Digest/aggregateInfo.R')
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rff = function(x)
{
  if(is.na(x))
    return(as.character(x))
  else
    return(formatC(as.numeric(x),format="f",digits = 2,big.mark = ",",big.interval = 3L))
}
rff1 = function(x)
{
  if(is.na(x))
    return(as.character(x))
  else
    return(formatC(as.numeric(x),format="f",digits = 1,big.mark = ",",big.interval = 3L))
}
rff3 = function(x)
{
  if(is.na(x))
    return(as.character(x))
  else
    return(formatC(as.numeric(x),format="f",digits = 3,big.mark = ",",big.interval = 3L))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

preparebody = function(path)
{
  print("Enter Body Func")
  body = ""
  body = paste(body,"Site Name: BESCOM\n",sep="")
  body = paste(body,"\nLocation: Hubli, Karnataka, India\n",sep="")
  body = paste(body,"\nO&M Code: IN-036\n",sep="")
  body = paste(body,"\nSystem Size: 4511 kWp\n",sep="")
  body = paste(body,"\nNumber of Energy Meters: 2\n",sep="")
  body = paste(body,"\nModule Brand / Model / Nos: JA Solar / 325Wp / 13880\n",sep="")
  body = paste(body,"\nInverter Brand / Model / Nos: Huawei / SUN2000_43KTL / 47\n",sep="")
  body = paste(body,"\nInverter Brand / Model / Nos: ABB / PVS800-57-1000KW-c / 2\n",sep="")
  body = paste(body,"\nSite COD: 2017-12-29\n",sep="")
  body = paste(body,"\nSystem age [days]: ",(20+DAYSACTIVE),sep="")
  body = paste(body,"\n\nSystem age [years]: ",round((20+DAYSACTIVE)/365,2),sep="")
  
  
  for(x in 1 : length(path))
  {
    print(path[x])
    dataread = read.table(path[x],header = T,sep="\t")
    fulgen=(as.numeric(dataread[1,21])+as.numeric(dataread[1,22]))
    fulyld=(as.numeric(fulgen)/4511)
    fulpr=((as.numeric(fulyld)*100)/as.numeric(dataread[1,30]))
    body = paste(body,"\n\n_____________________________________________\n",sep="")
    days = unlist(strsplit(path[x],"/"))
    days = days[length(days)]
    days = unlist(strsplit(days," "))
    days = unlist(strsplit(days[2],"\\."))
    body = paste(body,days[1],sep="")
    body = paste(body,"\t\tFull Site \n",sep="")
    body  = paste(body,"______________________________________________",sep="")
    body = paste(body,"\nPts Recorded/Data Availability: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
    body = paste(body,"\n\nFull Site generation [kWh]: ",rff(fulgen),sep = "")
    body = paste(body,"\n\nFull Site Yield [kWh/kWp]: ",rff1(fulyld),sep = "")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor [kWh/m2]: ",rff(dataread[1,30]),sep = "")
    body = paste(body,"\n\nFull Site PR [%]: ",rff1(fulpr),sep = "")
    body = paste(body,"\n\nStdDev/CoV [%]: ",rff(dataread[1,36]),"/",rff1(dataread[1,37]),sep = "")
    body = paste(body,"\n\nGrid Availability (STR) [%]: ",as.character(dataread[,27]),sep="")
    body = paste(body,"\n\nGrid Availability (CTR) [%]: ",as.character(dataread[,28]),sep="")
    body = paste(body,"\n\n_____________________________________________\n",sep="")
    days = unlist(strsplit(path[x],"/"))
    days = days[length(days)]
    days = unlist(strsplit(days," "))
    days = unlist(strsplit(days[2],"\\."))
    body = paste(body,days[1],sep="")
    body = paste(body,"\t\tWeather Parameters\n",sep="")
    body  = paste(body,"______________________________________________\n",sep="")
    body = paste(body,"\nDaily Irradiation Pyranometer, SMP [kWh/m2]: ",rff(dataread[1,4]),sep = "")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor, Cleaned [kWh/m2]: ",rff(dataread[1,30]),sep = "")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor, Dirty [kWh/m2]: ",rff(dataread[1,31]),sep = "")
    sicdrat=rff3(abs((as.numeric(dataread[1,31])/as.numeric(dataread[1,30]))))
    body = paste(body,"\n\nSoiling Ratio [Dirty/Clean]: ",as.character(sicdrat),"%",sep = "")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor, GHI [kWh/m2]: ",rff(dataread[1,3]),sep = "")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor, NW [kWh/m2]: ",rff(dataread[1,32]),sep = "")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor, SE [kWh/m2]: ",rff(dataread[1,33]),sep = "")
    body = paste(body,"\n\nMean Ambient Temperature [C]: ",rff1(dataread[1,5]),sep="")
    body = paste(body,"\n\nMean Ambient Temperature, sun hours [C]: ",rff1(dataread[1,6]),sep="")
    body = paste(body,"\n\nMean Humidity [%]: ",rff1(dataread[1,11]),sep="")
    body = paste(body,"\n\nMean Humidity, sun hours [%]: ",rff1(dataread[1,12]),sep="")
    body = paste(body,"\n\nMean Module Temperature NW, sun hours [C]: ",rff1(dataread[1,19]),sep="")
    body = paste(body,"\n\nMean Module Temperature SE, sun hours [C]: ",rff1(dataread[1,20]),sep="")
    body = paste(body,"\n\n SMP/GHI ratio: ",rff(dataread[1,41]),sep = "")
    body = paste(body,"\n\n SMP/Clean Si sensor ratio: ",rff(dataread[1,42]),sep = "")
    
    # ###############    STR
    body = paste(body,"\n\n_____________________________________________\n",sep="")
    days = unlist(strsplit(path[x],"/"))
    days = days[length(days)]
    days = unlist(strsplit(days," "))
    days = unlist(strsplit(days[2],"\\."))
    body = paste(body,days[1],sep="")
    body = paste(body,"\t\tBESCOM STR\n",sep="")
    body  = paste(body,"_____________________________________________\n",sep="")
    body = paste(body,"\nPts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor [kWh/m2]: ",rff(dataread[1,30]),sep="")
    body = paste(body,"\n\nSolar Energy Generated [kWh]: ",rff(dataread[1,21]),sep="")
    body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",rff(dataread[1,34]),sep="")
    prstr = rff1((as.numeric(dataread[1,34])*100)/as.numeric(dataread[1,30]))
    body = paste(body,"\n\nPR [%]: ",as.character(prstr),sep="")
    body = paste(body,"\n\nGrid Availability [%]: ",as.character(dataread[,27]),sep="")
    if(is.na(dataread[1,25]))
    {
      body = paste(body,"\n\nLast recorded time: ",as.character(dataread[1,25]),sep="")
      body = paste(body,"\n\nLast recorded valid time: ",as.character(dataread[1,39]),sep="")
    }
    else
      body = paste(body,"\n\nLast recorded time: ",as.character(dataread[1,25]),sep="") 
    
    body = paste(body,"\n\nLast recorded net-delivered reading [kWh]: ",rff(dataread[1,29]),sep="")
    body = paste(body,"\n\nEac-2 [kWh]: ",rff(dataread[1,43]),sep="")
    body = paste(body,"\n\nYield-2 [kWh/kWp]: ",rff(dataread[1,44]),sep="")
    body = paste(body,"\n\nLast recorded energy reading [kWh]: ",rff(dataread[1,45]),sep="")
    body = paste(body,"\n\nLast recorded time: ",as.character(dataread[1,46]),sep="")
    #body = paste(body,"\n\nLast recorded true solar reading [kWh]: ",rff(dataread[1,29]),sep="")
    
    # #######################    CTR
    body = paste(body,"\n\n_____________________________________________\n",sep="")
    days = unlist(strsplit(path[x],"/"))
    days = days[length(days)]
    days = unlist(strsplit(days," "))
    days = unlist(strsplit(days[2],"\\."))
    body = paste(body,days[1],sep="")
    body = paste(body,"\t\tBESCOM CTR\n",sep="")
    body  = paste(body,"_____________________________________________\n",sep="")
    body = paste(body,"\nPts Recorded/Data Availability: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor, cleaned [kWh/m2]: ",rff(dataread[1,30]),sep="")
    body = paste(body,"\n\nSolar Energy Generated [kWh]: ",rff(dataread[1,22]),sep="")
    body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",rff(dataread[1,35]),sep="")
    prctr = rff1((as.numeric(dataread[1,35])*100)/as.numeric(dataread[1,30]))
    body = paste(body,"\n\nPR [%]: ",as.character(prctr),sep="")
    body = paste(body,"\n\nGrid Availability [%]: ",as.character(dataread[,28]),sep="")
    if(is.na(dataread[1,38]))
    {
      body = paste(body,"\n\nLast recorded time: ",as.character(dataread[1,38]),sep="")
      body = paste(body,"\n\nLast recorded valid time: ",as.character(dataread[1,40]),sep="")
    }
    else
      body = paste(body,"\n\nLast recorded time: ",as.character(dataread[1,38]),sep="") 
    
    body = paste(body,"\n\nLast recorded reading [kWh]: ",rff(dataread[1,24]),sep="")
    body = paste(body,"\n\nEac-2 [kWh]: ",rff(dataread[1,47]),sep="")
    body = paste(body,"\n\nYield-2 [kWh/kWp]: ",rff(dataread[1,48]),sep="")
    body = paste(body,"\n\nLast recorded energy reading [kWh]: ",rff(dataread[1,49]),sep="")
    body = paste(body,"\n\nLast recorded time: ",as.character(dataread[1,50]),sep="")
    
  }
  print('Daily Summary for Body Done')
  body = paste(body,"\n\n_____________________________________________\n\n",sep="")
  
  body = paste(body,"Station History",sep="")
  body = paste(body,"\n_____________________________________________\n\n",sep="")
  
  body = paste(body,"Station Birth Date: ",dobstation)
  body = paste(body,"\n\n Days station active: ",DAYSACTIVE)
  body = paste(body,"\n\n Years station active: ",rf1(DAYSACTIVE/365))
  body = paste(body,"\n\nTotal irradiation from Gsi00 sensor this month [kWh/m2]: ",MONTHLYIRR1,sep="")
  body = paste(body,"\n\nTotal irradiation from Pyranometer this month [kWh/m2]: ",MONTHLYIRR2,sep="")
  body = paste(body,"\n\nTotal irradiation from Si Clean sensor this month [kWh/m2]: ",MONTHLYIRR3,sep="")
  body = paste(body,"\n\nTotal irradiation from Si Dirty sensor this month [kWh/m2]: ",MONTHLYIRR4,sep="")
  body = paste(body,"\n\nAverage irradiation from Gsi00 sensor this month [kWh/m2]: ",MONTHAVG1,sep="")
  body = paste(body,"\n\nAverage irradiation from Pyranometer this month [kWh/m2]: ",MONTHAVG2,sep="")
  body = paste(body,"\n\nForecasted irradiation total for Gsi00 sensor this month [kWh/m2]: ",FORECASTIRR1,sep="");
  body = paste(body,"\n\nForecasted irradiance total for Pyranometer this month [kWh/m2]: ",FORECASTIRR2,sep="");
  body = paste(body,"\n\nIrradiation total last 30 days from Gsi00 sensor [kWh/m2]: ",LAST30DAYSTOT,sep="")
  body = paste(body,"\n\nIrradiation average last 30 days from Gsi00 sensor [kWh/m2]: ",LAST30DAYSMEAN,sep="")
  body = paste(body,"\n\nSMP/GSi loss (%): ",SOILINGDEC,sep="")
  body = paste(body,"\n\nSMP/GSi loss per day (%): ",SOILINGDECPD,sep="")
  print('3G Data Done')	
  return(body)
}
path = "/home/admin/Dropbox/Cleantechsolar/1min/[718]"    #Changed the path
pathwrite = "/home/admin/Dropbox/Second Gen/[IN-036S]"
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[IN-036S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
if(!grepl("txt",currday))
{
  currday = prevday = dir(prevpathmonth)[(length(dir(prevpathmonth))-1)] 
}
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")


stnnickName = "718"
stnnickName2 = "IN-036S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
  yr = substr(lastdatemail,1,4)
  monyr = substr(lastdatemail,1,7)
  prevpathyear = paste(path,yr,sep="/")
  prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
  prevpathmonth = paste(prevpathyear,monyr,sep="/")
  currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
  prevwriteyears = paste(pathwrite,yr,sep="/")
  prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}
# Mail section
check = 0
sender <- "operations@cleantechsolar.com" 
uname <- 'shravan.karthik@cleantechsolar.com'
pwd = 'CTS&*(789'
recipients <- getRecipients("IN-036S","m")
wt =1
while(1)
{
	recipients <- getRecipients("IN-036S","m")
  recordTimeMaster("IN-036S","Bot")
  years = dir(path)
  writeyears = paste(pathwrite,years[length(years)],sep="/")
  checkdir(writeyears)
  pathyear = paste(path,years[length(years)],sep="/")
  months = dir(pathyear)
  writemonths = paste(writeyears,months[length(months)],sep="/")
  sumname = paste("[IN-036S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
  checkdir(writemonths)
  pathmonth = paste(pathyear,months[length(months)],sep="/")
  tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
  currday = dir(pathmonth)[length(dir(pathmonth))]
  if(!grepl("txt",currday))
  {
    currday = dir(prevpathmonth)[(length(dir(prevpathmonth))-1)] 
  }
  if(prevday == currday)
  {
    if(wt == 1)
    {
      print("")
      print("")
      print("__________________________________________________________")
      print(substr(currday,7,16))
      print("__________________________________________________________")
      print("")
      print("")
      print('Waiting')
      wt = 0
    }
    {
      if(tm > 1319 || tm < 30)
      {
        Sys.sleep(120)
      }        
      else
      {
        Sys.sleep(3600)
      }
    }
    next
  }
  print('New Data found.. sleeping to ensure sync complete')
  Sys.sleep(20)
  print('Sleep Done')
  if(prevpathmonth != pathmonth)
  {
    monthlyirr1 = monthlyirr2 = monthlyirr3 = monthlyirr4= 0
    strng = unlist(strsplit(months[length(months)],"-"))
    daystruemonth = nodays(strng[1],strng[2])
    dayssofarmonth = 0
  }
  
  dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t")
  datawrite = prepareSumm(dataread)
  datasum = rewriteSumm(datawrite)
  currdayw = gsub("718","IN-036S",currday)
  write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
  
  
  
  monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3])
  monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4])
  monthlyirr3 = monthlyirr3 + as.numeric(dataread[1,30])
  monthlyirr4 = monthlyirr4 + as.numeric(dataread[1,31])
  
  globirr1 = globirr1 + as.numeric(datawrite[1,3])
  globirr2 = globirr2 + as.numeric(datawrite[1,4])
  
  
  dayssofarmonth = dayssofarmonth + 1    
  daysactive = daysactive + 1
  last30days[[last30daysidx]] = as.numeric(datawrite[1,3])
  
  
  
  print('Digest Written');
  {
    if(!file.exists(paste(writemonths,sumname,sep="/")))
    {
      write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      print('Summary file created')
    }
    else 
    {
      write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
      print('Summary file updated')  
    }
  }
  filetosendpath = c(paste(writemonths,currdayw,sep="/"))
  filename = c(currday)
  filedesc = c("Daily Digest")
  dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t")
  datawrite = prepareSumm(dataread)
  prevdayw = gsub('718','IN-036S',prevday)
  dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t")
  print('Summary read')
  if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
  {
    print('Difference in old file noted')
    
    
    
    previdx = (last30daysidx - 1) %% 31
    
    if(previdx == 0) {previdx = 1}
    datasum = rewriteSumm(datawrite)
    prevdayw = gsub("718","IN-036S",prevday)
    write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    
    
    if(prevpathmonth == pathmonth)
    {
      monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
      monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
      monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,30]) - as.numeric(dataprev[1,30])
      monthlyirr4 = monthlyirr4 + as.numeric(datawrite[1,31]) - as.numeric(dataprev[1,31])
      
    }
    globirr1 = globirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
    globirr2 = globirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
    
    
    last30days[[previdx]] = as.numeric(datawrite[1,3])
    
    datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t")
    rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
    print(paste('Match found at row no',rn))
    datarw[rn,] = datasum[1,]
    write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
    filename[2] = prevdayw
    filedesc[2] = c("Updated Digest")
    print('Updated old files.. both digest and summary file')
  }
  
  last30daysidx = (last30daysidx + 1 ) %% 31
  if(last30daysidx == 0) {last30daysidx = 1}
  
  LAST30DAYSTOT = rf(sum(last30days))
  LAST30DAYSMEAN = rf(mean(last30days))
  DAYSACTIVE = daysactive
  MONTHLYIRR1 = monthlyirr1
  MONTHLYIRR2 = monthlyirr2
  MONTHLYIRR3 = monthlyirr3
  MONTHLYIRR4 = monthlyirr4
  
  MONTHAVG1 = rf(monthlyirr1 / dayssofarmonth)
  MONTHAVG2 = rf(monthlyirr2 / dayssofarmonth)
  
  
  FORECASTIRR1 = rf(as.numeric(MONTHAVG1) * daystruemonth)
  FORECASTIRR2 = rf(as.numeric(MONTHAVG2) * daystruemonth)
  
  
  SOILINGDEC = rf3(((globirr2 / globirr1) - 1) * 100)
  
  
  SOILINGDECPD = rf3(as.numeric(SOILINGDEC) / DAYSACTIVE)
  
  
  
  print('Sending mail')
  body = ""
  body = preparebody(filetosendpath)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-036S] Digest",substr(currday,7,16)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            file.names = filename,                # optional parameter
            file.descriptions = filedesc,     # optional parameter
            debug = F)
  print('Mail Sent');
  recordTimeMaster("IN-036S","Mail",substr(currday,7,16))
  prevday = currday
  prevpathyear = pathyear
  prevpathmonth = pathmonth
  prevwriteyears = writeyears
  prevwritemonths = writemonths
  prevsumname = sumname
  wt = 1
  gc()
}
sink()
