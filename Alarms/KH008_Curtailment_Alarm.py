import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import ast
import pyodbc

path='/home/admin/Dropbox/Gen 1 Data/[KH-008X]/'
mailpath='/home/admin/CODE/Send_mail/mail_recipients.csv'
tz=pytz.timezone('Asia/Phnom_Penh')

def sendmail(station,type,data,rec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients = rec# must be a list
    recipients.append('sai.pranav@cleantechsolar.com')
    TO=", ".join(recipients)
    if(type=='curtailment'):
        SUBJECT = station+' Curtailment Alarm'
    else:
        SUBJECT = station+' Zero Export Failure Alarm'
    text2='Last Timestamp Read: '+str(data) +'\n\n'
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()

def rectolist(station,path):
    df=pd.read_csv(path)
    df2=df[['Recipients',station]]
    a=df2.loc[df2[station] == 1]['Recipients'].tolist()
    return a

timestamps=[]
timestamps2=[]
while(1):
    time_now=datetime.datetime.now(tz)
    date_now=str(datetime.datetime.now(tz).date())
    if(os.path.exists(path+date_now[0:4]+'/'+date_now[0:7]+'/Load/[KH-008X-M6] '+date_now+'.txt')):
        df=pd.read_csv(path+date_now[0:4]+'/'+date_now[0:7]+'/Load/[KH-008X-M6] '+date_now+'.txt',sep='\t')
    else:
        print('File not present!')
        time.sleep(3600)
        continue
    active_power=df[['Local.Time.Stamp','Active.Power..W.']].dropna()
    for index, row in active_power.tail(60).iterrows():
        if(row['Active.Power..W.']<0):
            if(row['Local.Time.Stamp'] not in timestamps2):
                print(timestamps)
                timestamps2.append(row['Local.Time.Stamp'])
                recipients=rectolist('KH-008X_Mail',mailpath)
                print(row['Local.Time.Stamp'])
                print('Curtailment Alarm')
                sendmail('KH-008X','curtailment',row['Local.Time.Stamp'],recipients)
                break
    for index, row in active_power.tail(60).iterrows():
        if(row['Active.Power..W.']<0):
            if(row['Local.Time.Stamp'] not in timestamps2):
                print(timestamps)
                timestamps2.append(row['Local.Time.Stamp'])
    cnt=0
    flag=0
    for index, row in active_power.iterrows():
        if(row['Active.Power..W.']<0):
            cnt=cnt+1
            if(cnt==15):
                if(row['Local.Time.Stamp'] not in timestamps):
                    print(timestamps)
                    timestamps.append(row['Local.Time.Stamp'])
                    if(flag==0):
                        print(row['Local.Time.Stamp'])
                        print('Zero export failure alarm')
                        flag=1
                        recipients=rectolist('KH-008X_Mail',mailpath)
                        sendmail('KH-008X','Zero export',row['Local.Time.Stamp'],recipients)
                cnt=0
        else:
            cnt=0
    print('Sleeping!')
    time.sleep(3600)


