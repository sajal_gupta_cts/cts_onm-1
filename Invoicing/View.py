import requests, json
import requests.auth
import pandas as pd
import numpy as np
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import subprocess

pathread='/home/admin/Dropbox/HQDigest/MailDigest/HQ_'
pathwrite='/home/admin/Dropbox/HQDigest/Invoice/'
R_path="/home/admin/CODE/MasterMail/HQFunction.R"
path='/home/admin/Dropbox/Gen 1 Data/'
tz = pytz.timezone('Asia/Calcutta')

def tesco(timenowdate):
    stations=['[TH-010L]', '[TH-011L]', '[TH-012L]', '[TH-013L]', '[TH-014L]', '[TH-015L]', '[TH-016L]', '[TH-017L]', '[TH-018L]', '[TH-019L]', '[TH-020L]', '[TH-021L]', '[TH-022L]', '[TH-023L]', '[TH-024L]', '[TH-025L]', '[TH-026L]', '[TH-027L]', '[TH-028L]']
    substations=[['MFM_1_PV Meter'], ['MFM_1_PV meter'], ['MFM_2_PV Meter'], ['MFM_1_PV Meter'], ['MFM_1_PV Meter'], [ 'MFM_2_PV Meter',], ['MFM_1_PV Meter'], ['MFM_1_PV Meter'], ['MFM_1_PV Meter'], ['MFM_1_PV Meter'], [ 'MFM_1_PV Meter'], ['MFM_1_PV Meter'], ['MFM_1_PV Meter'], ['MFM_1_PV Meter'], [ 'MFM_1_PV MFM-1','MFM_2_PV MFM-2'], ['MFM_1_PV Meter'], [ 'MFM_1_PV Meter'], ['MFM_1_PV Meter'], ['MFM_2_PV Meter']]
    for j,i in enumerate(stations):
        temp=substations[j]
        print(i)
        for index,k in enumerate(temp):
            df=pd.read_csv(path+i+'/'+timenowdate[0:4]+'/'+timenowdate[0:7]+'/'+k+'/'+i+'-'+k[0:3]+k[4]+'-'+timenowdate[0:10]+'.txt',sep='\t')
            df_temp=df.iloc[240,[0,150]]
            df_temp['Operation']=i[1:-2]
            df_temp['ReadingInv']=round(df.iloc[240,150])
            if(i[1:-2]=='TH-024'):
                if(index==1):
                    print('First')
                    df_temp['MeterRef']=i[1:-2]+'B'
                elif(index==0):
                    print('Second')
                    df_temp['MeterRef']=i[1:-2]+'A'  
            else:
                df_temp['MeterRef']=i[1:-2]+'A'
            df_temp = df_temp[['Operation','MeterRef','ts','TotWhNet_max','ReadingInv']]
            df_temp.columns=['Operation','MeterRef','LastTime','LastRead','ReadingInv']
            df_temp=df_temp.fillna('NA')
            print(df_temp)
            frame = { 'Operation': df_temp['Operation'], 'MeterRef': df_temp['MeterRef'],'LastTime': df_temp['ts'],'LastRead': df_temp['TotWhNet_max'],'ReadingInv': df_temp['ReadingInv']} 
            result = pd.DataFrame(frame,columns=['Operation','MeterRef','LastTime','LastRead','ReadingInv'],index=[0]) 
            print(result)
            result.to_csv(pathwrite+'EOD_Meter_Reading_'+timenowdate+'.txt',sep='\t',index=False,mode='a',header=False)
while(1):
    hour_now=datetime.datetime.now(tz)
    subprocess.call (["Rscript",R_path, str(hour_now.date())]) #Calling HQFunction Script
    for i in range(15,0,-1):
        timenow=datetime.datetime.now(tz)-datetime.timedelta(days=i)
        timenowstr=str(timenow)
        timenowdate=str(timenow.date())
        print(pathread+timenowdate+'.txt')
        if(os.path.exists(pathread+timenowdate+'.txt')):
            df=pd.read_csv(pathread+timenowdate+'.txt',sep='\t')
            cols=['STN_ID','LastTime','LastRead']
            cols2=['Operation','MeterRef','LastTime','LastRead','ReadingInv']
            dic={'IN-009A':66491387, 'IN-011H': 255547365, 'IN-033A': 249863903, 'MY-006C': 29630715, 'KH-004A': 65020105, 'IN-038A': 198892638, 'MY-003A': 103305834, 'KH-002A': 96501488, 'IN-006F': 80539116, 'IN-029B': 170707720, 'KH-003A': 201198513, 'MY-002C': 195334159, 'IN-018A': 166453050, 'IN-014A': 118502725, 'MY-007A': 136874744, 'IN-026E': 97279934, 'IN-006G': 227136384, 'IN-061A': 217020431, 'IN-006B': 211700096, 'IN-072B': 238695700,  
                'IN-016C': 8302258, 'MY-002A': 127088733, 'IN-033B': 32680799, 'IN-012I': 19178394, 'IN-052A': 27233350, 'KH-007A': 231129464, 'IN-027E': 5826334, 'KH-008E': 249914809, 'KH-008A': 143340654, 'IN-062D': 116842936, 'IN-040A': 147291791, 'IN-019A': 116156557, 'IN-046A': 9258894, 'IN-032A': 127449025, 'IN-063A': 184288187, 'MY-004A': 244270336, 'IN-004A': 184259216, 'IN-011G': 128151056,'VN-001A':84447275,'IN-035A':49861062,'IN-035B':178091269,'VN-003A':259788225,'VN-002A':123039081,
                'IN-048A': 206260167,'IN-048B': 82202625,'IN-048C': 29387364, 'IN-047A': 135914712, 'KH-004B': 107164226, 'IN-004B': 61818957, 'IN-049A': 194700022, 'IN-034A': 183205614, 'IN-014C': 246037119, 'IN-062C': 133952521, 'IN-062B': 131379634, 'MY-006B': 124447825, 'MY-003D': 114220163, 'IN-044C': 65070011, 'IN-006C': 252875262, 'KH-008B': 30039218, 'IN-067A': 83293426, 'IN-062A': 47843824,'IN-081A':144647215,'IN-082A': 267471991,'IN-083A':112144942,'IN-083B':202711836,
                'IN-005D': 152221282, 'IN-006D': 72498818, 'IN-028E': 53577111, 'SG-001A': 202664138, 'IN-073C': 173949396, 'MY-003B': 43157479, 'IN-007C': 135685393, 'KH-002B': 203719220, 'IN-037A': 14736474, 'IN-026D': 69308121, 'IN-026B': 114660105, 'KH-008D': 130197737, 'IN-012G': 225554850, 'MY-001A': 153926356, 'MY-006A': 29915088, 'IN-045A': 53251520, 'IN-044B': 74790255, 'IN-068B': 83361813, 'IN-023D': 119007313, 
                'KH-005A': 35640351, 'SG-002A': 266885971, 'IN-037B': 135984095, 'IN-026A': 21997048, 'IN-066A': 113137141, 'IN-017A': 154506488, 'IN-039F': 134601544, 'IN-016B': 97427528, 'IN-026F': 76690209, 'IN-020B': 183292630, 'IN-006A': 148803787, 'IN-012F': 5761260, 'IN-031B': 180878109, 'IN-011F': 60780382, 'IN-056B': 199895023, 'IN-056A': 74479996, 'KH-001A': 216589185, 'IN-031A': 242298801, 'IN-021A': 41103235, 'IN-059A': 77398048, 'IN-016D': 221593453, 
                'IN-055C': 71595127, 'IN-071C': 116506250, 'IN-046B': 219526860, 'IN-016A': 263828986, 'IN-052B': 242795711, 'KH-008C': 115114241,'IN-014B': 84181069,'IN-053A':247077711,'IN-054C':27789542,'IN-055C':75605821,'IN-057A':103950268,'IN-057B':121893564,'IN-058A':159223701,'IN-058B':80823523,'IN-058C':63374806,'IN-060A':255193596,'IN-060B':51044092,'IN-075A':81964259,'MY-008A':78915256,'MY-008B':69559844,'MY-009A':198482996,
                'IN-041C':209835042,'IN-041B':237584235,'IN-042D':160975126,'IN-043A':200555279,'IN-074A':116366733,'IN-074B':110950548,'KH-001B':237490662,'KH-009C':89664146,'IN-064A':106020210,'IN-064B':107142956,'IN-064C':226910850,'IN-064D':238502934,'IN-064E':53017948,'KH-006A':24835983,'KH-006B':183516101,'MY-401A':56142694,'MY-402A':78025585,'MY-403A':74870957}
            final=pd.DataFrame(columns=cols2)
            for i in sorted(dic):
                print(i)
                a=df.loc[df['UniqID']==dic[i],cols]
                a=a.fillna(np.nan)
                a['STN_ID']=a['STN_ID'].str[:6]
                a.insert(1,'MeterRef', i)
                if(len(a['LastRead'].values)==0):
                    pass
                else:
                    a['ReadingInv']=round(a['LastRead'].values)
                    vals=a.values.tolist()
                    final.loc[len(final)] = vals[0]
            final=final.fillna('NA')
            final.to_csv(pathwrite+'EOD_Meter_Reading_'+timenowdate+'.txt',sep='\t',index=False)
            tesco(timenowdate)
        else:
            print('Waiting!')

    time.sleep(3600)
        
        