rm(list = ls())
require('ggplot2')
require('gridExtra')
path1 = '/home/admin/Dropbox/GIS/Summary/NewDelhi'

path2 = '/home/admin/Dropbox/Second Gen/711 Digest/2016'
path3 = '/home/admin/Dropbox/Second Gen/711 Digest/2017'
days = dir(path1)
days2 = dir(path2)
days = days[1:(length(days)-1)]
days2 = days2[2:length(days2)]
plotlist = vector('list',10)
x=9
cols = c('red','blue','green','orange','black','brown','darkolivegreen','coral4','darkslategrey','gray')
titles = c('2016-04','2016-05','2016-06','2016-07','2016-08','2016-09','2016-10','2016-11','2016-12','2017-01')
xaxiscomp=yaxiscomp=type=NULL
dateA=NULL
for(x in 1 : length(days))
{
	lenuse=30
  print(paste(x,'doing'))
	path1ac = paste(path1,days[x],sep="/")
	{
	if(x==10)
	{
		print('Inside 10 condition')
		daynew = dir(path3)
		print(daynew)
		path2ac = paste(path3,daynew[1],sep="/")
		lenuse=15
	}
	else
  {
		path2ac = paste(path2,days2[x],sep="/")
  }
	}
	daysin = dir(path2ac)
  path2ac = paste(path2ac,daysin[1],sep="/")
  dataread = read.table(path1ac,header = T,sep = "\t")
  dataread2 = read.table(path2ac,header = T,sep = "\t")
	dataread2 = dataread2[1:lenuse,]
  xaxis = as.numeric(dataread2[,4])
  yaxis = as.numeric(dataread[,2])
  xaxis = xaxis[1:lenuse]
  yaxis=yaxis[1:lenuse]
  rmse = xaxis-yaxis
  rmse = rmse ^ 2
  rmse = sum(rmse)/lenuse
  eqn=coef(lm(yaxis~xaxis))
  ynew=eqn[2]*xaxis+eqn[1]
  num=sum((ynew-yaxis)^2)
  den=sum((yaxis-mean(yaxis))^2)
  rmse=1-(num/den)
  df = data.frame(x = xaxis,y = yaxis) 
  plot = ggplot(df,aes(x = x,y=y)) + geom_point(col=cols[x],shape=x) + geom_abline(intercept = 0,color='black')
  plot = plot + ggtitle(titles[x])+ylab(bquote('Satellite Global Horizontal Irradiation [kWh/'*m^2*',daily]'))+xlab(bquote('Ground Global Horizontal Irradiation [kWh/'*m^2*',daily]'))+scale_x_continuous(breaks = seq(from=0,to=7,by=1),limits=c(0,7))
  plot = plot + scale_y_continuous(breaks=seq(from=0,to=7,by=1),limits=c(0,7))
  plot = plot + annotate('text',label = paste('R-sq error:',format(round(rmse,3),nsmall=3)),x=3,y=1)
  #plot = plot + geom_smooth(method="lm",se=F,color="black") 
  plotlist[[x]]<-plot
  xaxiscomp=rbind(xaxiscomp,cbind(xaxis))
  yaxiscomp=rbind(yaxiscomp,cbind(yaxis))
  type=rbind(type,cbind(unlist(rep(as.character(x),lenuse))))
  dateA=rbind(dateA,cbind(as.character(dataread[1:lenuse,1])))
}


pdf("/home/admin/InidividualMonthsDelhi.pdf",width = 20,height = 10)
grid.arrange(plotlist[[1]],plotlist[[2]],plotlist[[3]],plotlist[[4]],
            plotlist[[5]],plotlist[[6]],plotlist[[7]],plotlist[[8]],plotlist[[9]],
						plotlist[[10]],ncol=5) 
dev.off()

rmse = xaxiscomp-yaxiscomp
rmse = rmse ^ 2
rmse = sum(rmse)/length(xaxiscomp)
eqn=coef(lm(yaxiscomp~xaxiscomp))
ynew=eqn[2]*xaxiscomp+eqn[1]
num=sum((ynew-yaxiscomp)^2)
den=sum((yaxiscomp-mean(yaxiscomp))^2)
rmse=1-(num/den)
df2=data.frame(x=xaxiscomp,y=yaxiscomp,z=type)

plot2 = ggplot(data=df2,aes(x=xaxiscomp,y=yaxiscomp,shape=z,color=z)) + geom_point(size=3)
plot2 = plot2 + scale_color_manual(labels=c("1"="Apr","2"="May","3"="Jun","4"="Jul",
                                           "5"="Aug","6"="Sep","7"="Oct","8"="Nov","9"="Dec","10"="Jan"),values=cols)
plot2 = plot2 + scale_shape_manual(labels = c("1"="Apr","2"="May","3"="Jun","4"="Jul",
                                              "5"="Aug","6"="Sep","7"="Oct","8"="Nov","9"="Dec","10"="Jan"),
                                   values=c("1"=1,"2"=2,"3"=3,"4"=4,
                                            "5"=5,"6"=6,"7"=7,"8"=8,"9"=9,"10"=10))
plot2 = plot2 +xlab(bquote('Ground Global Horizontal Irradiation [kWh/'*m^2*',daily]')) + ylab(bquote('Satellite Global Horizontal Irradiation [kWh/'*m^2*',daily]')) 
plot2 = plot2 + scale_x_continuous(breaks = seq(from=0,to=7,by=1),limits=c(0,7))
plot2 = plot2 + scale_y_continuous(breaks=seq(from=0,to=7,by=1),limits=c(0,7))
plot2 = plot2 + theme(legend.position="bottom",legend.title=element_blank(),legend.key.size=unit(1,'cm'),legend.text=element_text(size=12),
                      axis.text=element_text(size=10),axis.title=element_text(size=15),panel.background = element_rect(fill = "gray93")) + geom_abline(intercept = 0)
plot2 = plot2 + annotate('text',label = paste('R-sq error:',format(round(rmse,3),nsmall=3)),x=3.5,y=1,size=8)
plot2


pdf('/home/admin/AggregateDelhi.pdf',width =8,height=8)
plot2
dev.off()


df2=data.frame(Date=dateA,SMP=xaxiscomp,GHI=yaxiscomp)
colnames(df2) = c('Date','SMP','GHI')
write.table(df2,file = '/home/admin/AggregateDelhi.txt',row.names = F,col.names=T,sep="\t",append =F)
