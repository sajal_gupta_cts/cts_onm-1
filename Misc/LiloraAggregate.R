rm(list = ls(all = T))

path = "/home/admin/Dropbox/GIS/Summary/Lilora"

pathWrite = "/home/admin/LiloraAggregate.txt"

days = dir(path)

date = c()
GHI = c()
idxglobal = 1
for(x in 1 : length(days))
{
	dataread = read.table(paste(path,days[x],sep="/"),header = T,sep = "\t")
	if(nrow(dataread) < 1)
		print(paste('Error in',days[x],'skipping'))
	for(z in 1:nrow(dataread))
	{
		datetmp = as.character(dataread[z,1])
		idxmatch = match(datetmp,date)
		if(is.finite(idxmatch))
			next
		idxmatch = length(date) + 1
		date[idxmatch] = datetmp
		GHI[idxmatch] = as.numeric(dataread[z,2])
	}
	print(days[x])
}

data = data.frame(Date=date,GHI=GHI,stringsAsFactors=F)

write.table(data,file=pathWrite,row.names =F,col.names=T,append=F,sep="\t")

