system('rm -R "/home/admin/Dropbox/Second Gen/[TH-001X]"')
system('rm -R "/home/admin/Dropbox/Third Gen/[TH-001X]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/TH001XDigest/TH001MailDigestFunctions.R')
source('/home/admin/CODE/Misc/calcBackLog.R')
path = '/home/admin/Dropbox/Gen 1 Data/[TH-001X]'
writepath2G = '/home/admin/Dropbox/Second Gen/[TH-001X]'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[TH-001X]'
checkdir(writepath3G)
DAYSALIVE = 0
years = dir(path)
stnnickName2 = "TH-001X"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[TH-001X-M1] ",lastdatemail,".txt",sep="")
ENDCALL=0

for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
    pathmonths = paste(pathyrs,months[y],sep="/")
    writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gmon)
    writepath3Gfinal = paste(writepath3Gyr,"/[TH-001X] ",months[y],".txt",sep="")
    stations = dir(pathmonths)
		stations = stations[c(2,3,1)] # By default order is Load, S1, S2 when it should be S1,S2,Load
		pathdays = paste(pathmonths,stations[1],sep="/")
    pathdays2 = paste(pathmonths,stations[2],sep="/")
    pathdays3 = paste(pathmonths,stations[3],sep="/")
    writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
    writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
    writepath2Gdays3 = paste(writepath2Gmon,stations[3],sep="/")
    checkdir(writepath2Gdays)
    checkdir(writepath2Gdays2)
    checkdir(writepath2Gdays3)
    days = dir(pathdays)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
    days2 = dir(pathdays2)
		days3 = dir(pathdays3)
		startt = 0
		if(length(days) != length(days2) || length(days) != length(days3))
		{
			minlen = min(length(days),length(days2),length(days3))
			days = tail(days,n=minlen)
			days2 = tail(days2,n=minlen)
			days3 = tail(days3,n=minlen)
		}
      for(t in 1 : length(days))
      {
        if(ENDCALL==1)
					break

				{
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
          else if(x == length(years) && y == length(months) && t == length(days))
          {
            next
          }
        }
        DAYSALIVE = DAYSALIVE + 1
        writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
        writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
        writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")
        readpath = paste(pathdays,days[t],sep="/")
        readpath2 = paste(pathdays2,days2[t],sep="/")
        readpath3 = paste(pathdays3,days3[t],sep="/")
				TOTSOLAR <<- 0
				METERCALLED <<- 1 #correct dont change
        df1 = secondGenData(readpath,writepath2Gfinal)
				METERCALLED <<- 2 # correct dont change
        df2 = secondGenData(readpath2,writepath2Gfinal2)
				METERCALLED <<- 3 # correct dont change
        df3 = secondGenData(readpath3,writepath2Gfinal3)
				invoiceData = computeInvoice(readpath)
      	thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath2Gfinal3,writepath3Gfinal,invoiceData)
				if(days[t] == stopDate)
				{
					ENDCALL = 1
				}
      }
			if(ENDCALL == 1)
				break
    }
			if(ENDCALL == 1)
				break
}
print('Historical Analysis Done')
