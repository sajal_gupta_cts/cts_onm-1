require('compiler')
checkdirUnOp = function(path)
{
if(!file.exists(path))
{
	dir.create(path)
}
}
checkdir = cmpfun(checkdirUnOp)

INSTCAP = c(1124.5,1131,52,45.5)
INSTCAPM = c(2255.5,2255.5)
NOMETERS=2
NOINVS=49
getGTIDataUnOp = function(date)
{
  yr = substr(date,1,4)
  yrmon = substr(date,1,7)
  filename = paste("[IN-036S]",date,".txt",sep="")
  filename2 = paste("[728]",date,".txt",sep="")
  path = "/home/admin/Dropbox/Second_Gen/[IN-036S]"
  path2 = "/home/admin/Dropbox/Cleantechsolar/1min/[728]"
  pathRead = paste(path,yr,yrmon,filename,sep="/")
  pathRead2= paste(path2,yr,yrmon,filename2,sep="/")
  GTIGreater20=NA
  GTI = DNI = NA
  if(file.exists(pathRead))
  {
    dataread = read.table(pathRead,sep="\t",header = T)
    if(nrow(dataread) > 0)
    {
      GTI = as.numeric(dataread[1,3])
      DNI = as.numeric(dataread[1,8])
    }
  }
  if(file.exists(pathRead2))#return all timestamps with irr>20
  {
      dataread = read.table(pathRead2,sep="\t",header = T,stringsAsFactors = F)
	  dataread=dataread[complete.cases(dataread[21]),]
      GTIGreater20=dataread[dataread[,21]>20,1]
    
  }
  return(c(GTI,DNI,GTIGreater20))
}
getGTIData = cmpfun(getGTIDataUnOp)

timetominsUnOp = function(time)
{
	hr = as.numeric(substr(time,12,13))
	min = as.numeric(substr(time,15,16))
	bucket =((hr*60) + min + 1)
	return(bucket)
}

timetomins = cmpfun(timetominsUnOp)

secondGenDataUnOp = function(pathread, pathwrite,type)
{
	dataread = try(read.table(pathread,sep="\t",header = T),silent = T)
	if(class(dataread) == "try-error")
	{
		print(paste('pathread error',pathread))
		date=DA=Eac1=Eac3=Eac2=Yld1=Yld2=LR=Tamb=Tmod=GTI=DNI=LT=TModSH=TambSH=PR1D=PR2D=PR1=PR2=GA=PA=IA=NA
		{
		if(type == 0)
			data = data.frame(Date = date,DA = DA,Eac1 = Eac1, Eac2 = Eac2, Yld1 = Yld1,
			Yld2 = Yld2, PR1=PR1,PR2=PR2, LastRead = LR, LastTime = LT, GA = GA, PA=PA)
		else if(type == 1)
			data = data.frame(Date = date,DA = DA,Eac1 = Eac1, EacAC = Eac2, EacDC = Eac3,
			Yld1 = Yld1, Yld2 = Yld2,LastRead = LR, LastTime = LT, IA=IA)
		else if(type == 2)
			data = data.frame(Date = date,DA = DA,GTI = GTI, Tamb = Tamb, Tmod = Tmod,TambSH=TambSH,TModSH=TModSH,MaxWS=WS)
		}
		write.table(data,file=pathwrite,row.names =F,col.names = T,sep="\t",append=F)
		thirdGenData(pathwrite)
		fourthGenData(pathwrite)
		return()	
	}
	if(nrow(dataread) != 0){
	dataread=replace(dataread, dataread=="NULL", NA)
	}
	date = substr(dataread[1,1],1,10)
	DA = format(round((nrow(dataread)/2.88),1),nsmall=1)
	{
		if(type == 0)
		{
			idxPower = 32
			idxEnergy = 66
			Eac1 = Eac2 = Yld1 = Yld2 = LR = LT =PR1 = PR2 = PR1D = PR2D = GA = PA = NA
			idxmtchdt = 1
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy])
			dataInt2 = as.character(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),1])

			dataIntTr = round(dataInt/1000000,0)
			dataIntTr2 = unique(dataIntTr)
			mode = dataIntTr2[which.max(tabulate(match(dataIntTr, dataIntTr2)))]
			dataInt = dataInt[abs(dataIntTr - mode) <= 100]
			dataInt2 = dataInt2[abs(dataIntTr - mode) <= 100]

			if(length(dataInt))
			{
				Eac2 = round((abs(dataInt[length(dataInt)] - dataInt[1])),2)
				LR = dataInt[length(dataInt)]
				LT = dataInt2[length(dataInt2)]
			}
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPower])),idxPower])
			dataInt = dataInt[abs(dataInt) < 1000000]
			if(length(dataInt))
				Eac1 = format(round((sum(dataInt)/12),2),nsmall=2)
			
			Yld1 = round(as.numeric(Eac1)/INSTCAPM[idxmtchdt],2)
			Yld2 = round(as.numeric(Eac2)/INSTCAPM[idxmtchdt],2)
			GTI = getGTIData(date)
			GTIGreater20=GTI[-c(1, 2)]
            if(length(GTIGreater20))
			{
				dataread1=dataread[complete.cases(dataread[11]),]
				VoltageGreater=dataread1[dataread1[,11]>150,1]#Found timestamps having Avg Voltage>150 here
				dataread2=dataread[complete.cases(dataread[32]),]
				PowerGreater=dataread2[dataread2[,32]>2,1]#Found timestamps having Power>2 here
				common=intersect(GTIGreater20,VoltageGreater)
				common2=intersect(GTIGreater20,PowerGreater)
				GA=(length(common)/length(GTIGreater20))*100
				GA=round(GA,1)
				PA=(length(common2)/length(GTIGreater20))*100
				PA=round(PA,1)
			} 
			DNI = as.numeric(GTI[2])
			GTI = as.numeric(GTI[1])
			PR1 = round(Yld1*100/GTI,1)
			PR2 = round(Yld2*100/GTI,1)
			data = data.frame(Date = date,DA = DA,Eac1 = Eac1, Eac2 = Eac2, Yld1 = Yld1,
			Yld2 = Yld2, PR1=PR1, PR2 = PR2, LastRead = LR,LastTime = LT,PR1D=PR1D,PR2D=PR2D,GA=GA,PA=PA)
		}
		else if(type == 1)
		{
			idxEnergy = 33
			idxPowerAC = 15
			idxPowerDC = 21
			Eac1 = Eac2 = Eac3 = LR = LT=IA=NA
			GTI = getGTIData(date)
			GTIGreater20=GTI[-c(1, 2)] 
            if(length(GTIGreater20))
			{
				dataread=dataread[complete.cases(dataread[21]),]
				DCPowerGreater2=dataread[dataread[,21]>0,1]#Found timestamps having DCPower>0 here
				common=intersect(GTIGreater20,DCPowerGreater2)
				IA=(length(common)/length(GTIGreater20))*100
				IA=round(IA,1)
			}
			idxmtchdt = 3
			{
			if(grepl("C_Inverter_1",pathread))
				idxmtchdt = 1
			else if(grepl("C_Inverter_2",pathread))
				idxmtchdt = 2
			else if(grepl("S_Inverter",pathread))
			{
				mtno = unlist(strsplit(pathread,"S_Inverter_"))
				mtno = unlist(strsplit(mtno[2],"/"))
				mtno = as.numeric(mtno[1])
				if(is.finite(mtno) && mtno > 18)
					idxmtchdt = 4
			}
			}
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy])
			dataInt2 = as.character(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),1])
			
			dataIntTr = round(dataInt/1000000,0)
			dataIntTr2 = unique(dataIntTr)
			mode = dataIntTr2[which.max(tabulate(match(dataIntTr, dataIntTr2)))]
			dataInt = dataInt[abs(dataIntTr - mode) <= 1]
			dataInt2 = dataInt2[abs(dataIntTr - mode) <= 1]
			
			if(length(dataInt))
			{
				Eac2 = round((abs(dataInt[length(dataInt)] - dataInt[1])),2)
				LR = dataInt[length(dataInt)]
				LT = dataInt2[length(dataInt2)]
			}
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPowerAC])),idxPowerAC])
			dataInt = dataInt[abs(dataInt) < 1000000]
			if(length(dataInt))
				Eac1 = format(round((sum(dataInt)/12),2),nsmall=2)
			
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPowerDC])),idxPowerDC])
			dataInt = dataInt[abs(dataInt) < 1000000]
			if(length(dataInt))
				Eac3 = format(round((sum(dataInt)/12),2),nsmall=2)

			Yld1 = round(as.numeric(Eac1)/INSTCAP[idxmtchdt],2)
			Yld2 = round(as.numeric(Eac2)/INSTCAP[idxmtchdt],2)
			
			data = data.frame(Date = date,DA = DA,Eac1 = Eac1, EacAC = Eac2, EacDC = Eac3,
			Yld1 = Yld1, Yld2 = Yld2, LastRead = LR, LastTime = LT,IA=IA)
		}
		else if(type == 2)
		{
			idxGTI = 21
			idxTMod = 10
			idxTamb = 13
			idxWS = 12
			Tamb = Tmod = GTI = TModSH=TambSH=WS=NA
			
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxWS])),idxWS])
			if(length(dataInt))
				WS = round(max(dataInt),1)
			
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxTMod])),idxTMod])
			if(length(dataInt))
				Tmod = round(mean(dataInt),1)
			
			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxTamb])),idxTamb])
			if(length(dataInt))
				Tamb = round(mean(dataInt),1)

			dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxGTI])),idxGTI])
			if(length(dataInt))
				GTI = format(round((sum(dataInt)/12000),2),nsmall=2)
			dataInt = dataread[complete.cases(as.numeric(dataread[,idxTamb])),c(1,idxTamb)]
			if(length(dataInt[,1]))
			{
				tmmins = timetomins(as.character(dataInt[,1]))
				dataInt = dataInt[tmmins < 1140,]
				tmmins = tmmins[tmmins < 1140]
				if(length(tmmins))
				{
					dataInt = dataInt[tmmins > 420,2]
					if(length(dataInt))
						 TambSH= format(round(mean(as.numeric(dataInt)),1),nsmall=1)
				}
			}
			dataInt = dataread[complete.cases(as.numeric(dataread[,idxTMod])),c(1,idxTMod)]
			if(length(dataInt[,1]))
			{
				tmmins = timetomins(as.character(dataInt[,1]))
				dataInt = dataInt[tmmins < 1140,]
				tmmins = tmmins[tmmins < 1140]
				if(length(tmmins))
				{
					dataInt = dataInt[tmmins > 420,2]
					if(length(dataInt))
						TModSH = format(round(mean(as.numeric(dataInt)),1),nsmall=1)
				}
			}
			data = data.frame(Date = date,DA = DA,GTI = GTI, Tamb = Tamb, Tmod = Tmod,
			TambSH = TambSH,TModSH=TModSH,MaxWS = WS)
		}
	}
	write.table(data,file=pathwrite,row.names =F,col.names = T,sep="\t",append=F)
	print("done writing")
	thirdGenData(pathwrite)
	fourthGenData(pathwrite)
}

secondGenData = cmpfun(secondGenDataUnOp)

thirdGenDataUnOp = function(path2G)
{
		stnnames = unlist(strsplit(path2G,"/"))
		path3G = paste("/home/admin/Dropbox/FlexiMC_Data/Third_Gen",stnnames[7],sep="/")
		checkdir(path3G)
		path3GStn = paste(path3G,stnnames[10],sep="/")
		checkdir(path3GStn)
		pathwrite3GFinal = paste(path3GStn,paste(substr(stnnames[9],1,18),".txt",sep=""),sep="/")
		dataread = read.table(path2G,header = T,sep ="\t")
		{
			if(!file.exists(pathwrite3GFinal))
				write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=T,append=F,sep="\t")
			else
				write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=F,append=T,sep="\t")
		}
		dataread = read.table(pathwrite3GFinal,header = T,sep="\t")
		tm = as.character(dataread[,1])
		tmuniq = unique(tm)
		# The idea here is update third-gen with the most recent readings. So if 
		# there are multiple calls, only take the latest row for the day and 
		# slot that into the third-gen data
		if(length(tm) > length(tmuniq))
		{
			dataread = dataread[-(nrow(dataread)-1),]
		}
		write.table(dataread,file=pathwrite3GFinal,row.names=F,col.names=T,append=F,sep="\t")
}

thirdGenData = cmpfun(thirdGenDataUnOp)

fourthGenDataUnOp = function(path2G)
{
	order = c("WMS","CI_MFM","SI_MFM","C_Inverter_1","C_Inverter_2","S_Inverter_1",
	"S_Inverter_2","S_Inverter_3","S_Inverter_4","S_Inverter_5","S_Inverter_6",
	"S_Inverter_7","S_Inverter_8","S_Inverter_9","S_Inverter_10","S_Inverter_11",
	"S_Inverter_12","S_Inverter_13","S_Inverter_14","S_Inverter_15","S_Inverter_16",
	"S_Inverter_17","S_Inverter_18","S_Inverter_19","S_Inverter_20","S_Inverter_21",
	"S_Inverter_22","S_Inverter_23","S_Inverter_24","S_Inverter_25","S_Inverter_26",
	"S_Inverter_27","S_Inverter_28","S_Inverter_29","S_Inverter_30","S_Inverter_31",
	"S_Inverter_32","S_Inverter_33","S_Inverter_34","S_Inverter_35","S_Inverter_36",
	"S_Inverter_37","S_Inverter_38","S_Inverter_39","S_Inverter_40","S_Inverter_41",
	"S_Inverter_42","S_Inverter_43","S_Inverter_44","S_Inverter_45","S_Inverter_46","S_Inverter_47")
	CONSTANTLEN = c(7,rep(9,NOMETERS),unlist(rep(8,(NOINVS)))) # No of columns for each file excluding date
	stnnames = unlist(strsplit(path2G,"/"))
	rowtemplate = unlist(rep(NA,sum(CONSTANTLEN)+1))
	path4G = paste("/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen",stnnames[7],sep="/")
	checkdir(path4G)
	dataread = read.table(path2G,header = T,sep ="\t")
	pathfinal = paste(path4G,paste(stnnames[7],"-lifetime.txt",sep=""),sep="/")
	idxmtch = match(stnnames[10],order)
	start = 2
	if(idxmtch > 1)
	{
		start = 2 + sum(CONSTANTLEN[1:(idxmtch -1)])
	}
	end = start + CONSTANTLEN[idxmtch] - 1
	idxuse = start : end
	colnames2G = colnames(dataread)
	colnames2G = colnames2G[-1]
	colnames2G = paste(stnnames[10],colnames2G,sep="-")

	{
		if(!file.exists(pathfinal))
		{
			colnames = rep("Date",length(rowtemplate))
			colnames[idxuse] = colnames2G
			rowtemplate[idxuse] = dataread[1,(2:ncol(dataread))]
			rowtemplate[1] = as.character(dataread[1,1])
			dataExist = data.frame(rowtemplate)
			colnames(dataExist) = colnames
			write.table(dataExist,file=pathfinal,row.names =F,col.names=T,sep="\t",append=F)
		}
		else
		{
			dataExist = read.table(pathfinal,sep="\t",header=T)
			colnames = colnames(dataExist)
			colnames[idxuse] = colnames2G
			dates = as.character(dataExist[,1])
			idxmtchdt = match(as.character(dataread[,1]),dates)
			if(is.finite(idxmtchdt))
			{
				tmp = dataExist[idxmtchdt,]
				tmp[idxuse] = dataread[1,2:ncol(dataread)]
				dataExist[idxmtchdt,] = tmp
				colnames(dataExist) = colnames
				write.table(dataExist,file=pathfinal,row.names =F,col.names=T,sep="\t",append=F)
			}
			else
			{
				rowtemplate = unlist(rowtemplate)
				rowtemplate[c(1,idxuse)] = dataread[1,]
				dataExist = data.frame(rowtemplate)
				colnames(dataExist) = colnames
				write.table(dataExist,file=pathfinal,row.names =F,col.names=F,sep="\t",append=T)
			}
		}
	}

	print(paste("4G Done",path2G))
}

fourthGenData = cmpfun(fourthGenDataUnOp)
