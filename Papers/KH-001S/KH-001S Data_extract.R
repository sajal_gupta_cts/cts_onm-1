rm(list=ls(all =TRUE))

pathRead <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Raw Data/[KH-001S]"
pathWrite <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Data Extracted/KH-001S_PR_summary.txt"
pathWrite2 <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Data Extracted/KH-001S_PR_summary.csv"
setwd(pathRead)                                     #set working directory
print("Extracting data..")
filelist <- dir(pattern = ".txt", recursive= TRUE)  #contains only files of '.txt' format

nameofStation <- "KH-001S"

col0 <- c()
col1 <- c()
col2 <- c()


col0[10^6] = col1[10^6] = col2[10^6] = 0


index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")  #reading text file at location filelist[i]
  
  for (j in 1:nrow(temp)){
    
    col0[index] <- nameofStation
    
    col1[index] <- paste0(temp[j,1])
    
    col2[index] <- as.numeric(temp[j,15])
    
    index <- index + 1
    
  }
  
  print(paste(i, "done"))
}

col0 <- col0[1:(index-1)]   #removes last row for all columns
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col1[is.na(col1)] <- NA              #states T/F
col2[is.na(col2)] <- NA
#col2[c(940,564,563,516,567,159,562,156,463,62,110,64,55,2,66,106,3,114,116,68,111)] = NA 

exclude_index <- 1
for(i in col2){
  if(isTRUE (i < 60)){  # eliminate PR below 60%
    col2[exclude_index] <- NA
  }
  
  if(isTRUE (i > 100)){   # eliminate PR above 100%
    col2[exclude_index] <- NA
  }
  exclude_index <- exclude_index +1
}


print("Starting to save..")

result <- cbind(col0,col1,col2)
colnames(result) <- c("Meter Reference","Date","PR")         #columns names


for(x in c(3)){       
  result[,x] <- round(as.numeric(result[,x]),1)
  
}

rownames(result) <- NULL
result <- data.frame(result) 
#result <- result[-c(1:120),] # remove row 1 to 120
for(i in 1:nrow(result)){
  result[i,4] <- i
}
colnames(result) <- c("Meter Reference","Date","PR","No. of Days")
write.table(result,na = "",pathWrite,row.names = FALSE,sep ="\t")   #saving into txt and csv file
write.csv(result,pathWrite2, na= "", row.names = FALSE)