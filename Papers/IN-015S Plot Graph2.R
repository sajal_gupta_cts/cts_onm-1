rm(list=ls(all =TRUE))

library(ggplot2)
library(reshape)
library(lubridate)
library(devtools)
library(grid)

pathWrite <- "/home/admin/Graphs/"
result <- read.csv("/tmp/IN-015S_PR_summary.csv",stringsAsFactors = F)

rownames(result) <- NULL
result <- data.frame(result)
result$Date <- as.POSIXct(result$Date, format = "%Y-%m-%d")

avg = mean(result$PR,na.rm=TRUE)
std = sd(result$PR,na.rm=TRUE)
sigma2POS = avg + (1*std)
sigma2NEG = avg - (1*std)
print(sigma2POS)
print(sigma2NEG)
No_of_points <- nrow(result)

m <- lm(result$PR ~ result$No..of.Days)
a <- signif(coef(m)[1], digits = 2)
b <- signif(coef(m)[2], digits = 2)
equation <- paste("y = ",a," ", b," x", sep="")


PRGraph <- ggplot(data=result,aes(x=No..of.Days,y=PR)) + geom_point() + theme_bw() + #theme_classic() +
#  stat_smooth(method = lm, se = FALSE, show.legend = TRUE) +
  scale_y_continuous(limits = c(50, 90), expand= c(0,0), breaks=seq(50,90,10)) +
  scale_x_continuous(limits = c(0, 920), expand= c(0,0),breaks=seq(0,920,100)) +
 # ggtitle(expression("[SG-001X] Lifetime Performance Ratio (without 2"*sigma~"filter)"), subtitle = paste0("From ",result$Date[1]," to ",result$Date[nrow(result)])) +
  theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(10,0,-30,0)),
        plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5, margin = margin(0,0,-40,0))) +
  ylab("Performance Ratio [%]") + 
  xlab("# of Days") 
PRGraph <- PRGraph +  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
 # theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
	 theme(axis.title.x = element_blank(), axis.text.x = element_blank())
  #theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
 # geom_hline(yintercept=sigma2POS, linetype="dashed") +
 # geom_hline(yintercept=sigma2NEG, linetype="dashed") 

#PRGraph <- PRGraph +  annotate("text",label = expression(paste("+2"*sigma~"[81.6%]")),size = 3,  #update the value(%) according to Sigma2POS
#           x = 100, y= sigma2POS+5,fontface =1,hjust = 0) 
#PRGraph <- PRGraph +  annotate("text",label = paste("+2 sigma [81.6%]"),size = 3,  #update the value(%) according to Sigma2POS
#           x = 100, y= sigma2POS+5,fontface =1,hjust = 0) 
#PRGraph <- PRGraph +  annotate("text",label = paste("+2*sigma ~ 81.6"),size = 3, parse=T, #update the value(%) according to Sigma2POS
#           x = 175, y= sigma2POS+2.5,fontface =1,hjust = 0) 
#PRGraph <- PRGraph +   annotate("text",label = paste("-2*sigma ~ 72.6"),size = 3,parse=T,   #update the value(%) according to Sigma2NEG
#           x = 175, y= sigma2NEG-2.5,fontface =1,hjust = 0) 
# PRGraph <- PRGraph +  annotate("text",label = paste0("Avg PR = ",sprintf("%0.1f", avg),"%"),size = 3,
#           x = 550, y= 69.7,fontface =1,hjust = 0) 
#  PRGraph <- PRGraph + annotate("text",label = paste0("Degradation = ", sprintf("%0.1f", b*365) ,"% p.a"),size = 3,
#           x = 550, y= 65.7,fontface =1,hjust = 0) 
# PRGraph <- PRGraph +  annotate("text",label = paste0(equation),size = 3,
#           x = 550, y= 89,fontface =1,hjust = 0) 
# PRGraph <- PRGraph +  annotate("text",label = paste0("Start PR = ",sprintf("%0.1f", a), "%"),size = 3,
#           x = 550, y= 87.5,fontface =1,hjust = 0) 
# PRGraph <- PRGraph +  annotate("text",label = paste0("Current PR = ",sprintf("%0.1f", a+No_of_points*b),"%"),size = 3,
#           x = 550, y= 83.5,fontface =1,hjust = 0) 
 PRGraph <- PRGraph +  theme(plot.margin = unit(c(0.5,0.5,0.5,0.2),"cm"))  #top, right, bottom, left
PRGraphOg <- PRGraph 
ggsave(PRGraph,filename = paste0(pathWrite,"SG-001X_lifetime_PR_Without_Filter.pdf"), width = 7.92, height = 5) 
print("saved")

count <- 0
exclude_index <- c()
for(i in 1:nrow(result)){
  if(isTRUE (result[i,3] < sigma2NEG)){  # eliminate PR below -2Sigma
    exclude_index <- c(exclude_index,i)
    count <- count + 1
  }
  
  if(isTRUE (result[i,3] > sigma2POS)){   # eliminate PR above +2Sigma
    exclude_index <- c(exclude_index,i)
    count <- count + 1
  }
}
result[exclude_index,3] <- NA

m <- lm(result$PR ~ result$No..of.Days)
a <- signif(coef(m)[1], digits = 2)
b <- signif(coef(m)[2], digits = 2)
equation <- paste("y = ",a," ", b," x", sep="")

PRGraph <- ggplot(data=result,aes(x=No..of.Days,y=PR)) + geom_point() +theme_bw() + #theme_classic() +
  stat_smooth(method = lm, se = FALSE, show.legend = TRUE) +
  scale_y_continuous(limits = c(50, 90), expand= c(0,0), breaks=seq(50,90,10)) +
  scale_x_continuous(limits = c(0, 920), expand= c(0,0),breaks=seq(0,920,100)) +
 # ggtitle(paste("[SG-001X] Lifetime Performance Ratio"), subtitle = paste0("From ",result$Date[1]," to ",result$Date[nrow(result)])) +
  theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(30,0,-30,0)),
        plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5, margin = margin(30,0,-40,0))) +
  ylab("Performance Ratio [%]") + 
  xlab("# of Days") +
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(2,2,0,0)))+
#	 theme(axis.title.x = element_blank(), axis.text.x = element_blank())+
  geom_hline(yintercept=sigma2POS, linetype="dashed") +
  geom_hline(yintercept=sigma2NEG, linetype="dashed") + 
#  annotate("text",label = paste("+2*sigma ~ 81.6"),size = 3,parse=T,  #update the value(%) according to Sigma2POS 
#            x = 100, y= sigma2POS+2.5,fontface =1,hjust = 0) +
  #annotate("text",label = paste(expression(+2*sigma ~ *"["*81.6 *"%]")),size = 3,parse=T,  #update the value(%) according to Sigma2POS 
  annotate("text",label = paste(expression(+1*sigma ~ " [86.1%]")),size = 3,parse=T,  #update the value(%) according to Sigma2POS 
            x = 100, y= sigma2POS+2.5,fontface =1,hjust = 0) +
  annotate("text",label = paste(expression(-1*sigma ~ " [63.9%]")),size = 3, parse=T,  #update the value(%) according to Sigma2NEG 
           x = 100, y= sigma2NEG-2.5,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Avg PR = ",sprintf("%0.1f", avg),"%"),size = 3,
           x = 300, y= 57.0,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Degradation = ", sprintf("%0.1f", b*365) ,"% p.a"),size = 3,
           x = 300, y= 52,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Number of points available = ", nrow(result)-count),size = 3,
           x = 570, y= 59.7,fontface =1,hjust = 0) +    
  annotate("text",label = paste("Points eliminated = 38"),size = 3, #update the value(%) according to count
           x = 570, y= 55.2,fontface =1,hjust = 0) +
  annotate("text",label = paste0("% of points eliminated = ",round(count/No_of_points*100,1)," %"),size = 3,
           x = 570, y= 51.2,fontface =1,hjust = 0) +
#  annotate("text",label = paste0(equation),size = 3,
#          x = 550, y= 85.5,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Start PR = ",sprintf("%0.1f", a), "%"),size = 3,
           x = 0, y= 57,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Current PR = ",sprintf("%0.1f", a+No_of_points*b),"%"),size = 3,
           x = 0, y= 52,fontface =1,hjust = 0) +
  theme(plot.margin = unit(c(0.0,0.5,0.2,0.2),"cm")) #top, right, bottom, left


ggsave(PRGraph,filename = paste0(pathWrite,"SG-001X_lifetime_PR.pdf"), width = 7.92, height = 5) 

grid.newpage()
pdf("/home/admin/Graphs/Combo1SDIN.pdf",width=5.92,height=5)
grid.draw(rbind(ggplotGrob(PRGraphOg), ggplotGrob(PRGraph), size = "last"))
dev.off()
