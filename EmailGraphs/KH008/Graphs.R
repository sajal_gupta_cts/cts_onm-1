#Remember to install this package for text alignment using install.packages("DescTools")
library(MASS)

tempList = list()
myList = list()
dateTimeList = list()
waterLevelList = list()

yearFiles = list.files(path = "/home/admin/Dropbox/Cleantechsolar/1min/[728]",all.files = F, full.names = T)
idx = match("_gsdata_",yearFiles)
if(is.finite(idx))
  yearFiles = yearFiles[-idx]
numOfYears = length(yearFiles) - 1

dailyPondVal = 0
dailyR02Val = 0
dailyR03Val = 0
dailyR06Val = 0
dailyR11Val = 0

readData = function() {
  numOfFiles = length(files)
  for (x in 1:numOfFiles) {
    #extract data from .txt file
    data = read.table(files[x], header = T, sep = "\t")
    
    #extract data from table
    dataDate = substr(files[x], (nchar(files[x]) - 13), (nchar(files[x]) - 4))
    numOfRows = length(data$Rec_ID)
    DAPercentage = round(100 * (numOfRows / 1440), digits = 1)
    SMPList = data$AvgSMP10
    GsiList = data$AvgGsi00_Shr
    SMPVal = Reduce("+",lapply(SMPList, as.numeric))/60000
    GsiVal = Reduce("+",lapply(GsiList, as.numeric))/60000
    ratioVal = SMPVal/GsiVal
    
    startPond = 1
    while(toString(data$Act_E.Del_Pond[startPond]) == "NaN" && startPond < length(data$Act_E.Del_Pond)) {
      startPond = startPond + 1
    }
    endPond = length(data$Act_E.Del_Pond)
    while(toString(data$Act_E.Del_Pond[endPond]) == "NaN" && endPond > 0) {
      endPond = endPond - 1
    }
    if (startPond > endPond) {
      PondPR = NA
      dailyPondVal = NA
    }
    else {
      PondPR = 100 * ((as.numeric(data$Act_E.Del_Pond[endPond]) - as.numeric(data$Act_E.Del_Pond[startPond]))/2835.32)/GsiVal
      dailyPondVal = as.numeric(data$Act_E.Del_Pond[endPond]) - as.numeric(data$Act_E.Del_Pond[startPond])      
    }
    
    startR02 = 1
    while(toString(data$Act_E.Del_R02[startR02]) == "NaN" && startR02 < length(data$Act_E.Del_R02)) {
      startR02 = startR02 + 1
    }
    endR02 = length(data$Act_E.Del_R02)
    while(toString(data$Act_E.Del_R02[endR02]) == "NaN" && endR02 > 0) {
      endR02 = endR02 - 1
    }
    if (startR02 > endR02) {
      R02PR = NA
      dailyR02Val = NA
    }
    else {
      R02PR = 100 * ((as.numeric(data$Act_E.Del_R02[endR02]) - as.numeric(data$Act_E.Del_R02[startR02]))/2249.98)/GsiVal
      dailyR02Val = as.numeric(data$Act_E.Del_R02[endR02]) - as.numeric(data$Act_E.Del_R02[startR02])      
    }
    
    startR03 = 1
    while(toString(data$Act_E.Del_R03[startR03]) == "NaN" && startR03 < length(data$Act_E.Del_R03)) {
      startR03 = startR03 + 1
    }
    endR03 = length(data$Act_E.Del_R03)
    while(toString(data$Act_E.Del_R03[endR03]) == "NaN" && endR03 > 0) {
      endR03 = endR03 - 1
    }
    if (startR03 > endR03) {
      R03PR = NA
      dailyR03Val = NA
    }
    else {
      R03PR = 100 * ((as.numeric(data$Act_E.Del_R03[endR03]) - as.numeric(data$Act_E.Del_R03[startR03]))/2854.80)/GsiVal
      dailyR03Val = as.numeric(data$Act_E.Del_R03[endR03]) - as.numeric(data$Act_E.Del_R03[startR03])      
    }
    
    startR06 = 1
    while(toString(data$Act_E.Del_R06[startR06]) == "NaN" && startR06 < length(data$Act_E.Del_R06)) {
      startR06 = startR06 + 1
    }
    endR06 = length(data$Act_E.Del_R06)
    while(toString(data$Act_E.Del_R06[endR06]) == "NaN" && endR06 > 0) {
      endR06 = endR06 - 1
    }
    if (startR06 > endR06) {
      R06PR = NA
      dailyR06Val = NA
    }
    else {
      R06PR = 100 * ((as.numeric(data$Act_E.Del_R06[endR06]) - as.numeric(data$Act_E.Del_R06[startR06]))/495.95)/GsiVal
      dailyR06Val = as.numeric(data$Act_E.Del_R06[endR06]) - as.numeric(data$Act_E.Del_R06[startR06])      
    }
    
    startR11 = 1
    while(toString(data$Act_E.Del_R11[startR11]) == "NaN" && startR11 < length(data$Act_E.Del_R11)) {
      startR11 = startR11 + 1
    }
    endR11 = length(data$Act_E.Del_R11)
    while(toString(data$Act_E.Del_R11[endR11]) == "NaN" && endR11 > 0) {
      endR11 = endR11 - 1
    }
    if (startR11 > endR11) {
      R11PR = NA
      dailyR11Val = NA
    }
    else {
      R11PR = 100 * ((as.numeric(data$Act_E.Del_R11[endR11]) - as.numeric(data$Act_E.Del_R11[startR11]))/1397.50)/GsiVal
      dailyR11Val = as.numeric(data$Act_E.Del_R11[endR11]) - as.numeric(data$Act_E.Del_R11[startR11])      
    }
    
    EACTotal = dailyPondVal + dailyR02Val + dailyR03Val + dailyR06Val + dailyR11Val
    yieldTotal = EACTotal/9833.55
    totalPR = 100 * (yieldTotal/GsiVal)
    
    waterLevel = mean(unlist(lapply(na.omit(data$AvgWaterLvl),as.numeric)))
    
    tempList <<- append(tempList,c(dataDate, SMPVal, GsiVal, ratioVal, PondPR, R02PR, R03PR, R06PR, R11PR, DAPercentage, EACTotal, yieldTotal, totalPR, waterLevel))
    
    dateTimeList <<- append(dateTimeList, unlist(lapply(data$Tm,toString)))
    waterLevelList <<- append(waterLevelList, unlist(data$AvgWaterLvl))
  }
}

extractData = function() {
  for (m in 1:12) {
    if (m < 10) {
		pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[728]/",toString(y),"/",toString(y),"-","0",toString(m))
      if (file.exists(pathprobe)) {
        files <<- list.files(path = pathprobe,
                             all.files = F, full.names = T)
        readData()
        
        #adding info to list
        myList <<- append(myList,tempList)
        tempList <<- list()
      }
      else {
        print(paste("File",pathprobe," does not exist!"))
      }
    }
    else {
			pathprobe = (paste0("/home/admin/Dropbox/Cleantechsolar/1min/[728]/",toString(y),"/",toString(y),"-",toString(m)))
      if (file.exists(pathprobe)) {
        files <<- list.files(path = pathprobe,
                             all.files = F, full.names = T)
        readData()
        
        #adding info to list
        myList <<- append(myList,tempList)
        tempList <<- list()
      }
      else {
        print(paste("File",pathprobe," does not exist!"))
      }
    }
  }
}

#loop to begin extracting data
for (y in 2019:(2018+numOfYears)) {
  extractData()
}

#obtaining satellite data
satData = c(unlist(read.table("/home/admin/Dropbox/GIS/Summary/CMIC/CMIC Aggregate.txt", header = T, sep = "\t")$GHI[416:418]) ,
            unlist(read.table("/home/admin/Dropbox/GIS/Summary/CMIC/CMIC Aggregate.txt", 
                              header = T, sep = "\t")$GHI[449:length(read.table("/home/admin/Dropbox/GIS/Summary/CMIC/CMIC Aggregate.txt", 
                                                                                header = T, sep = "\t")$GHI)]))

#combining all data into a matrix
numDataPoints = length(myList)/14
usefulDataMatrix = matrix(nrow = (numDataPoints + 1), ncol = 16)
usefulDataMatrix[1,] = c("Date","Pyranometer","Silicon","Ratio (SMP/Gsi)","Satellite","Pond PR","R02 PR","R03 PR","R06 PR",
                         "R11 PR","Data Availability","EAC Total","Yield Total","PR (Total)", "Ratio (Sat/Pyr)", "Average Water Level")

#get .txt output for usefulData
for(z in 1:numDataPoints) {
  usefulDataMatrix[(z+1),] = c(unlist(myList[14*(z-1) + 1]), unlist(myList[14*(z-1) + 2]), unlist(myList[14*(z-1) + 3]), unlist(myList[14*(z-1) + 4]), satData[z], unlist(myList[14*(z-1) + 5]), unlist(myList[14*(z-1) + 6]), unlist(myList[14*(z-1) + 7]), unlist(myList[14*(z-1) + 8]), unlist(myList[14*(z-1) + 9]), unlist(myList[14*(z-1) + 10]), unlist(myList[14*(z-1) + 11]), unlist(myList[14*(z-1) + 12]), unlist(myList[14*(z-1) + 13]), (as.numeric(satData[z])/as.numeric(unlist(myList[14*(z-1) + 2]))), unlist(myList[14*z]))
}

#save .txt file to the right directory
setwd("/home/admin/Jason/cec intern/results/KH008S")
write.matrix(usefulDataMatrix, "allDataKH008.txt", sep = "\t")
pdf("Rplots.pdf",width=11,height=8)
#plot Data Availability
plotDataAvailability = function() {
  #plot time series graphs
  plot(unlist(lapply(usefulDataMatrix[6:(length(usefulDataMatrix[,1]) - 1),1],as.Date)),usefulDataMatrix[6:(length(usefulDataMatrix[,1]) - 1),11], type = "l", xaxt = "n", xlab = "", ylab = "Data Availability [%]", col = "black", lty = 1, las = 1, cex.lab = 1.3)
  
  #re-lable time-axis
  axis.Date(1, at = seq(from = as.Date(usefulDataMatrix[6,1]), to = as.Date(usefulDataMatrix[(length(usefulDataMatrix[,1]) - 1),1]), by = "month"), format = "%b/%y")
  
  #get information about data
  averageDA = mean(unlist(lapply(usefulDataMatrix[6:(dim(usefulDataMatrix)[1] - 1),11], as.numeric)))
  lifespanSoFar = difftime(as.Date(usefulDataMatrix[(dim(usefulDataMatrix)[1] - 1),1]), as.Date(usefulDataMatrix[6,1]), units = c("days"))
  
  #add information about average DA to plot
  text(x = (0.23*par("usr")[1] + 0.77*par("usr")[2]), y = 95, paste0("Average data availability = ",format(round(averageDA, digits = 1), nsmall = 1),"%","\n","Lifetime = ",lifespanSoFar," days (",round(lifespanSoFar/365,digits = 1)," years)"), cex = 0.8, adj = 0)
  
  #add titles
  title(main = paste("[KH-008S] Data Availability"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[6,1], " to ", usefulDataMatrix[(dim(usefulDataMatrix)[1] - 1),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("G01-DA-KH-008S","\n","Dhruv Baid, ",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotDataAvailability()

#plot Daily Irradiation Bars
plotIrradiationBars = function() {
  #Plot daily irradiation bars based on Silicon sensor
  myPlot = barplot(unlist(lapply(usefulDataMatrix[7:length(usefulDataMatrix[,1]),3],as.numeric)), las = 2, mgp = c(3,0.6,0), xlab = "", ylab = expression('Daily Global Horizontal Irradiation - Silicon [kWh/m'^2*']'), ylim = c(0,7.3), xaxt = "n", cex.lab = 1.3, mgp = c(2.5,0.6,-0.2))
  
  #index of dates in mmm/YY format
  index_y = lapply(lapply(usefulDataMatrix[7:dim(usefulDataMatrix)[1],1], as.Date), format, "%b/%y")
  
  #actual labels for date-axis (i.e. non-duplicated ones)
  dateLabels = index_y[!duplicated(index_y)]
  
  #list of points on the default x-axis where the new labels should go (i.e. points in the list of dates which were not duplicates - start of year)
  at_tick = which(!duplicated(index_y))
  
  #labelling time-axis  
  axis(side = 1, at = myPlot[at_tick] , labels = dateLabels, cex.axis = 0.4, las = 2, mgp = c(3,0.75,0))
  
  #calculate average daily irradiation
  avgDailyIrradiation = mean(unlist(lapply(usefulDataMatrix[7:dim(usefulDataMatrix)[1],3],as.numeric)))
  annualAverage = 365 * avgDailyIrradiation
  
  #plot line to show average daily irradiation
  abline(h = avgDailyIrradiation, lwd = 2)
  
  #add text to explain
  text(x = (0.55*par("usr")[1] + 0.45*par("usr")[2]), y = (0.03*par("usr")[3] + 0.97*par("usr")[4]), bquote("Daily Average = "*.(format(round(avgDailyIrradiation,digits = 2), nsmall = 2))~"kWh/m"^2), adj = 0, cex = 0.8, font = 2)
  text(x = (0.55*par("usr")[1] + 0.45*par("usr")[2]), y = (0.05*par("usr")[3] + 0.95*par("usr")[4]), bquote("Annual Average = "*.(format(round(annualAverage,digits = 0), nsmall = 0, big.mark = ","))~"kWh/m"^2), adj = 0, cex = 0.8, font = 2)
  
  #add titles
  title(main = paste("[KH-008S] Daily Irradiation"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[7,1], " to ", usefulDataMatrix[dim(usefulDataMatrix)[1],1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("G02-GHI-KH-008S","\n","Dhruv Baid, ",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotIrradiationBars()

#plot Pyranometer/Silicon ratio values
plotRatioValues = function() {
  #defining 4 lists for different colors
  blueList = list()
  lightBlueList = list()
  orangeList = list()
  brownList = list()
  
  #sort ratio values into different colors by SMP10 value
  for (i in 114:dim(usefulDataMatrix)[1]) {
    if(as.numeric(usefulDataMatrix[i,2]) < 2) {
      blueList = append(blueList, c(usefulDataMatrix[i,1],usefulDataMatrix[i,4]))
    }
    else if (as.numeric(usefulDataMatrix[i,2]) >= 2 & as.numeric(usefulDataMatrix[i,2]) < 4) {
      lightBlueList = append(lightBlueList, c(usefulDataMatrix[i,1],usefulDataMatrix[i,4]))      
    }
    else if (as.numeric(usefulDataMatrix[i,2]) >= 4 & as.numeric(usefulDataMatrix[i,2]) < 6) {
      orangeList = append(orangeList, c(usefulDataMatrix[i,1],usefulDataMatrix[i,4]))      
    }
    else {
      brownList = append(brownList, c(usefulDataMatrix[i,1],usefulDataMatrix[i,4]))      
    }
  }
  
  #defining 4 matrices
  blueMatrix = matrix(nrow = length(blueList)/2, ncol = 2)
  lightBlueMatrix = matrix(nrow = length(lightBlueList)/2, ncol = 2)
  orangeMatrix = matrix(nrow = length(orangeList)/2, ncol = 2)
  brownMatrix = matrix(nrow = length(brownList)/2, ncol = 2)
  
  #populating these matrices
  for (i in 1:nrow(blueMatrix)) {
    blueMatrix[i,] = c(unlist(blueList[2*i - 1]), unlist(blueList[2*i]))
  }
  for (i in 1:nrow(lightBlueMatrix)) {
    lightBlueMatrix[i,] = c(unlist(lightBlueList[2*i - 1]), unlist(lightBlueList[2*i]))
  }
  for (i in 1:nrow(orangeMatrix)) {
    orangeMatrix[i,] = c(unlist(orangeList[2*i - 1]), unlist(orangeList[2*i]))
  }
  for (i in 1:nrow(brownMatrix)) {
    brownMatrix[i,] = c(unlist(brownList[2*i - 1]), unlist(brownList[2*i]))
  }
  
  #Plot Pyr/Si ratio values
  plot(unlist(lapply(blueMatrix[,1],as.Date)),blueMatrix[,2], xlab = "", ylab = "Pyranometer to Silicon Sensor Readings Ratio", xaxt = "n", xlim = c(as.Date(usefulDataMatrix[114,1]), as.Date(usefulDataMatrix[dim(usefulDataMatrix)[1],1])), ylim = c(0.8,1.2), cex.lab = 1.3, mgp = c(2.5,1,0), las = 1, col = "blue", pch = 15)
  points(unlist(lapply(lightBlueMatrix[,1],as.Date)),lightBlueMatrix[,2], xlab = "", ylab = "", xaxt = "n", col = "lightblue", pch = 16)
  points(unlist(lapply(orangeMatrix[1:dim(orangeMatrix)[1],1],as.Date)),orangeMatrix[1:dim(orangeMatrix)[1],2], xlab = "", ylab = "", xaxt = "n", col = "orange", pch = 17)
  points(unlist(lapply(brownMatrix[1:dim(brownMatrix)[1],1],as.Date)),brownMatrix[1:dim(brownMatrix)[1],2], xlab = "", ylab = "", xaxt = "n", col = "brown", pch = 18)
  
  #Adding a line through them
  lines(spline(unlist(lapply(usefulDataMatrix[114:length(usefulDataMatrix[,1]),1],as.Date)),usefulDataMatrix[114:length(usefulDataMatrix[,1]),4]), xlab = "", ylab = "", xaxt = "n")
  
  #add date-axis label
  axis.Date(1, at = seq(from = as.Date(usefulDataMatrix[114,1]), to = as.Date(usefulDataMatrix[length(usefulDataMatrix[,1]),1]) , by = "month"), format = "%b/%Y")
  
  #Add horizontal line at ratio = 1
  abline(h = 1, lwd = 2)
  
  #Add text to explain the horizontal line
  text(x = (0.9975*par("usr")[1] + 0.0025*par("usr")[2]), y = 0.99, "Ratio = 1.000", cex = 0.8, adj = 0)
  
  #calculate average ratio
  averageRatio = mean(unlist(lapply(usefulDataMatrix[114:dim(usefulDataMatrix)[1],4],as.numeric)))
  
  #add horizontal line for average ratio
  abline(h = averageRatio, lty = 2)
  
  #add text for average ratio
  text(x = (0.9975*par("usr")[1] + 0.0025*par("usr")[2]), y = averageRatio - 0.01, paste0("Average Ratio = ", format(round(averageRatio, digits = 3), nsmall = 3)), cex = 0.8, adj = 0)
  
  #legend
  text(x = (0.98*par("usr")[1] + 0.02*par("usr")[2]), y = 0.81, expression('Daily Irradiation [kWh/m'^2*']'), adj = 0)
  text(x = (0.74*par("usr")[1] + 0.26*par("usr")[2]), y = 0.81, "<2", adj = 0)
  text(x = (0.64*par("usr")[1] + 0.36*par("usr")[2]), y = 0.81, "2~4", adj = 0)
  text(x = (0.54*par("usr")[1] + 0.46*par("usr")[2]), y = 0.81, "4~6", adj = 0)
  text(x = (0.44*par("usr")[1] + 0.56*par("usr")[2]), y = 0.81, ">6", adj = 0)
  points((0.76*par("usr")[1] + 0.24*par("usr")[2]), 0.81, pch = 15, col = "blue")
  points((0.66*par("usr")[1] + 0.34*par("usr")[2]), 0.81, pch = 16, col = "lightblue")
  points((0.56*par("usr")[1] + 0.44*par("usr")[2]), 0.81, pch = 17, col = "orange")
  points((0.46*par("usr")[1] + 0.54*par("usr")[2]), 0.81, pch = 18, col = "brown")
  
  #add titles
  title(main = paste("[KH-008S] Pyranometer to Silicon Sensor Ratio"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[114,1], " to ", usefulDataMatrix[dim(usefulDataMatrix)[1],1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("G03-GHI-GSi00-Ratio-KH-008S","\n","Dhruv Baid, ",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotRatioValues()

#Force fit overall PR rogue point
usefulDataMatrix[114,14] = mean(as.numeric(usefulDataMatrix[113,14]),as.numeric(usefulDataMatrix[115,14]))

#plot PR values for 5 different meters
#defining necessary variables
counter = 0
sumPondPR = 0
sumR02PR = 0
sumR03PR = 0
sumR06PR = 0
sumR11PR = 0
sum30 = 0
sum60 = 0
sum90 = 0
sumTotal = 0
counter30 = 0
counter60 = 0
counter90 = 0
counterTotal = 0
plotPRPoints = function() {
  #Pond
  plot(unlist(lapply(usefulDataMatrix[7:dim(usefulDataMatrix)[1],1],as.Date)),usefulDataMatrix[7:dim(usefulDataMatrix)[1],6], pch = 15, col = "steelblue1", xlab = "", ylab = "Performance Ratio [%]", xaxt = "n", xlim = c(as.Date(usefulDataMatrix[7,1]),(as.Date(usefulDataMatrix[dim(usefulDataMatrix)[1],1]) + 3)), ylim = c(0,100), cex.lab = 1.3, mgp = c(2.5,1,0), las = 1)
  #R02
  points(unlist(lapply(usefulDataMatrix[7:dim(usefulDataMatrix)[1],1],as.Date)),usefulDataMatrix[7:dim(usefulDataMatrix)[1],7], pch = 16, col = "red")
  #R03
  points(unlist(lapply(usefulDataMatrix[7:dim(usefulDataMatrix)[1],1],as.Date)),usefulDataMatrix[7:dim(usefulDataMatrix)[1],8], pch = 17, col = "orange")
  #R06
  points(unlist(lapply(usefulDataMatrix[7:dim(usefulDataMatrix)[1],1],as.Date)),usefulDataMatrix[7:dim(usefulDataMatrix)[1],9], pch = 18, col = "black")
  #R11
  points(unlist(lapply(usefulDataMatrix[7:dim(usefulDataMatrix)[1],1],as.Date)),usefulDataMatrix[7:dim(usefulDataMatrix)[1],10], pch = 19, col = "purple")
  #Total, after forcing a different value for one of the PR points
  usefulDataMatrix[114,14] = 100 * (as.numeric(usefulDataMatrix[114,13])/as.numeric(usefulDataMatrix[114,5]))
  lines(spline(unlist(lapply(usefulDataMatrix[7:dim(usefulDataMatrix)[1],1],as.Date)),usefulDataMatrix[7:dim(usefulDataMatrix)[1],14]), lwd = 3, col = "blue")
  
  #add a point for the last available data point for PR (Total)
  points(unlist(lapply(usefulDataMatrix[dim(usefulDataMatrix)[1]:dim(usefulDataMatrix)[1],1],as.Date)),usefulDataMatrix[dim(usefulDataMatrix)[1]:dim(usefulDataMatrix)[1],14], pch = 4, col = "blue")
  #label this point
  text(x = (as.Date(usefulDataMatrix[dim(usefulDataMatrix)[1],1]) + 1), y = (as.numeric(usefulDataMatrix[dim(usefulDataMatrix)[1],14])), paste(format(round(as.numeric(usefulDataMatrix[dim(usefulDataMatrix)[1],14]),digits = 1), nsmall = 1),"%"), cex = 0.8, adj = 0, col= "blue")
  
  #label date-axis
  axis.Date(1, at = seq(from = as.Date(usefulDataMatrix[7,1]), to = as.Date(usefulDataMatrix[length(usefulDataMatrix[,1]),1]) , by = "month"), format = "%b/%y")
  
  #calculating average PRs for each meter
  for (i in 7:dim(usefulDataMatrix)[1]) {
    sumPondPR <<- ifelse(usefulDataMatrix[i,6] > 0, sumPondPR + as.numeric(usefulDataMatrix[i,6]), sumPondPR)
    counter <<- ifelse(usefulDataMatrix[i,6] > 0, counter + 1, counter)
  }
  avgPond = sumPondPR/counter
  counter <<- 0
  
  for (i in 7:dim(usefulDataMatrix)[1]) {
    sumR02PR <<- ifelse(usefulDataMatrix[i,7] > 0, sumR02PR + as.numeric(usefulDataMatrix[i,7]), sumR02PR)
    counter <<- ifelse(usefulDataMatrix[i,7] > 0, counter + 1, counter)
  }
  avgR02 = sumR02PR/counter
  counter <<- 0
  
  for (i in 7:dim(usefulDataMatrix)[1]) {
    sumR03PR <<- ifelse(usefulDataMatrix[i,8] > 0, sumR03PR + as.numeric(usefulDataMatrix[i,8]), sumR03PR)
    counter <<- ifelse(usefulDataMatrix[i,8] > 0, counter + 1, counter)
  }
  avgR03 = sumR03PR/counter
  counter <<- 0
  
  for (i in 7:dim(usefulDataMatrix)[1]) {
    sumR06PR <<- ifelse(usefulDataMatrix[i,9] > 0, sumR06PR + as.numeric(usefulDataMatrix[i,9]), sumR06PR)
    counter <<- ifelse(usefulDataMatrix[i,9] > 0, counter + 1, counter)
  }
  avgR06 = sumR06PR/counter
  counter <<- 0
  
  for (i in 7:dim(usefulDataMatrix)[1]) {
    sumR11PR <<- ifelse(usefulDataMatrix[i,10] > 0, sumR11PR + as.numeric(usefulDataMatrix[i,10]), sumR11PR)
    counter <<- ifelse(usefulDataMatrix[i,10] > 0, counter + 1, counter)
  }
  avgR11 = sumR11PR/counter
  counter <<- 0
  
  #plotting average PRs for each meter
  abline(h = avgPond, col = "steelblue1")
  abline(h = avgR02, col = "red")
  abline(h = avgR03, col = "orange")
  abline(h = avgR06, col = "black")
  abline(h = avgR11, col = "purple")
  
  #adding business plan average line
  abline(h = 78, col = rgb(0, 128, 0, maxColorValue = 255), lwd = 2, lty = 2)
  
  #add text to explain that line
  text(x = (0.99*par("usr")[1] + 0.01*par("usr")[2]), y = 100, "Performance Ratio = 78.0% (business plan)", col = rgb(0, 128, 0, maxColorValue = 255), adj = 0, cex = 0.8)
  
  #calculating averages for PR (Total)
  for (i in 1:30) {
    #print(i)
    if (isTRUE(as.numeric(usefulDataMatrix[(dim(usefulDataMatrix)[1] - i + 1),14]) < 100)) {
      sum30 <<- sum30 + as.numeric(usefulDataMatrix[(dim(usefulDataMatrix)[1] - i + 1),14])
      counter30 <<- counter30 + 1
    }
    else {
      print("lol")
    }
  }
  avg30 = sum30/counter30
  
  for (i in 1:60) {
    #print(i)
    if (isTRUE(as.numeric(usefulDataMatrix[(dim(usefulDataMatrix)[1] - i + 1),14]) < 100)) {
      sum60 <<- sum60 + as.numeric(usefulDataMatrix[(dim(usefulDataMatrix)[1] - i + 1),14])
      counter60 <<- counter60 + 1
    }
    else {
      print("lol")
    }
  }
  avg60 = sum60/counter60
  
  for (i in 1:90) {
    #print(i)
    if (isTRUE(as.numeric(usefulDataMatrix[(dim(usefulDataMatrix)[1] - i + 1),14]) < 100)) {
      sum90 <<- sum90 + as.numeric(usefulDataMatrix[(dim(usefulDataMatrix)[1] - i + 1),14])
      counter90 <<- counter90 + 1
    }
    else {
      print("lol")
    }
  }
  avg90 = sum90/counter90
  
  for (i in 7:length(usefulDataMatrix[,14])) {
    #print(i)
    if (isTRUE(as.numeric(usefulDataMatrix[i,14]) < 100)) {
      sumTotal <<- sumTotal + as.numeric(usefulDataMatrix[i,14])
      counterTotal <<- counterTotal + 1
    }
    else {
      print("lol")
    }
  }
  avgOverall = sumTotal/counterTotal
  
  #adding text for 30-, 60-, 90-day and overall averages for PR (Total)
  text(x = (0.75*par("usr")[1] + 0.25*par("usr")[2]), y = 10, paste0("30-day Average: ",format(round(avg30,digits = 1), nsmall = 1), "%","\n","60-day Average: ", format(round(avg60,digits = 1), nsmall = 1), "%","\n","90-day Average: ", format(round(avg90,digits = 1), nsmall = 1), "%","\n","Lifetime Average: ", format(round(avgOverall,digits = 1), nsmall = 1), "%"), adj = 0, cex = 0.8, col = "blue")
  
  #adding text for more information on size of each meter
  text(x = par("usr")[1] + 0.498*(par("usr")[2] - par("usr")[1]), y = 20, bquote(underline("Size of Meters")), cex = 0.8, adj = 0)
  overallText = cbind(c("R03: 2,854.80 kWp","Pond: 2,835.32 kWp","R02: 2,249.98 kWp","R11: 1,397.50 kWp","R06: 495.95 kWp","Total: 9,833.55 kWp"))
  text(x = par("usr")[1] + 0.5*(par("usr")[2] - par("usr")[1]), y = 16, paste0(overallText[1]), cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.5*(par("usr")[2] - par("usr")[1]), y = 13.5, paste0(overallText[2]), cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.5*(par("usr")[2] - par("usr")[1]), y = 11, paste0(overallText[3]), cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.5*(par("usr")[2] - par("usr")[1]), y = 8.5, paste0(overallText[4]), cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.5*(par("usr")[2] - par("usr")[1]), y = 6, paste0(overallText[5]), cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.5*(par("usr")[2] - par("usr")[1]), y = 3.5, paste0(overallText[6]), cex = 0.8, adj = 0, font = 2)
  text(x = par("usr")[1] + 0.624*(par("usr")[2] - par("usr")[1]), y = 16, " [29.0%]", cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.624*(par("usr")[2] - par("usr")[1]), y = 13.5, " [28.8%]", cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.624*(par("usr")[2] - par("usr")[1]), y = 11, " [22.9%]", cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.624*(par("usr")[2] - par("usr")[1]), y = 8.5, " [14.2%]", cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.624*(par("usr")[2] - par("usr")[1]), y = 6, " [5.0%]", cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.624*(par("usr")[2] - par("usr")[1]), y = 3.5, " [100.0%]", cex = 0.8, adj = 0, font = 2)
  
  #legend
  legend(x = c(par("usr")[2] - 0.31*(par("usr")[2] - par("usr")[1]),par("usr")[2]), y = c(40,par("usr")[3]), c(paste0("Pond [Average = ", round(avgPond, digits = 1),"%]"),paste0("R02 [Average = ", round(avgR02, digits = 1),"%]"),paste0("R03 [Average = ", round(avgR03, digits = 1),"%]"),paste0("R06 [Average = ", round(avgR06, digits = 1),"%]"),paste0("R11 [Average = ", round(avgR11, digits = 1),"%]"),paste0("PR (Total)")), pch = c(15,16,17,18,19,NA), lty = c(NA,NA,NA,NA,NA,1), lwd = c(NA,NA,NA,NA,NA,3), col = c("steelblue1","red","orange","black","purple","blue"), y.intersp = 0.8)
  
  #add titles
  title(main = paste("[KH-008S] Performance Ratios"), line = 2.2, cex.main = 1.5)
  title(main = paste0("From ", usefulDataMatrix[7,1], " to ", usefulDataMatrix[dim(usefulDataMatrix)[1],1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("G04-PR-KH-008S","\n","Dhruv Baid, ",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotPRPoints()

#forcefully remove rogue data points for satellite vs. ground data; might want to simply create a new matrix
filteredUsefulDataMatrix = usefulDataMatrix[-c(81,114),]

#save .txt file to the right directory
setwd("/home/admin/Jason/cec intern/results/KH008S")
write.matrix(filteredUsefulDataMatrix, "allDataFilteredKH008.txt", sep = "\t")

#plot satellite vs ground data
plotSatVsGround = function() {
  #plot points for pyranometer and silicon
  plot(filteredUsefulDataMatrix[7:(length(filteredUsefulDataMatrix[,1]) - 1),3],filteredUsefulDataMatrix[7:(length(filteredUsefulDataMatrix[,1]) - 1),5], xlim = c(0,7.7), ylim = c(0,7.7), xlab = expression('Daily Global Horizontal Irradiation - Ground (SMP10 or Gsi00) [kWh/m'^2*']'), ylab = expression('Daily Global Horizontal Irradiation - Satellite [kWh/m'^2*']'), col = "red", pch = 16, cex.lab = 1.3, mgp = c(2.5,1,0), las = 1)
  points(filteredUsefulDataMatrix[113:(length(filteredUsefulDataMatrix[,1]) - 1),2],filteredUsefulDataMatrix[113:(length(filteredUsefulDataMatrix[,1]) - 1),5], xaxt = "n", xlab = "", ylab = "", col = "blue", pch = 17)
  
  #add straight line of gradient 1 for reference
  abline(a = 0, b = 1, col = "black")
  
  #legend
  legend("topleft", c("Gsi00","SMP10"), pch = c(16,17), col = c("red","blue"))
  
  #get r-squared values
  r2Gsi00 = format(round(summary(lm(unlist(lapply(filteredUsefulDataMatrix[7:(dim(filteredUsefulDataMatrix)[1] - 1),5],as.numeric)) ~ unlist(lapply(filteredUsefulDataMatrix[7:(dim(filteredUsefulDataMatrix)[1] - 1),3],as.numeric))))$r.squared, digits = 3), nsmall = 3)
  r2SMP10 = format(round(summary(lm(unlist(lapply(filteredUsefulDataMatrix[113:(dim(filteredUsefulDataMatrix)[1] - 1),5],as.numeric)) ~ unlist(lapply(filteredUsefulDataMatrix[113:(dim(filteredUsefulDataMatrix)[1] - 1),2],as.numeric))))$r.squared, digits = 3), nsmall = 3)
  
  #print r-squared values onto plot
  text(x = (0.15*par("usr")[1] + 0.85*par("usr")[2]), y = (0.9*par("usr")[3] + 0.1*par("usr")[4]), bquote('R'^2 ~ " [Gsi00] =" ~ .(r2Gsi00)), adj = 0, cex = 0.8, col = "red")
  text(x = (0.15*par("usr")[1] + 0.85*par("usr")[2]), y = (0.95*par("usr")[3] + 0.05*par("usr")[4]), bquote('R'^2 ~ " [SMP10] =" ~ .(r2SMP10)), adj = 0, cex = 0.8, col = "blue")

  #add titles
  title(main = paste("[KH-008S] Satellite vs. Ground Irradiation"), line = 2.2, cex.main = 1.5)
  title(main = paste0("From ", usefulDataMatrix[7,1], " to ", usefulDataMatrix[(dim(usefulDataMatrix)[1] - 1),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("G05-SATvsGND-KH-008S","\n","Dhruv Baid, ",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotSatVsGround()

#Plot Pyr/Si Ratio against GHI (Silicon)
plotPyrSiVsSi = function() {
  #plot points for pyranometer and silicon
  plot(usefulDataMatrix[114:dim(usefulDataMatrix)[1],3],usefulDataMatrix[114:dim(usefulDataMatrix)[1],4], xlab = expression('Daily Global Horizontal Irradiation - Gsi00) [kWh/m'^2*']'), ylab = "Pyr/Si Ratio", xaxt = "n", yaxt = "n", xlim = c(0,8), ylim = c(0.95,1.10), col = "red", pch = 16, cex.lab = 1.3, mgp = c(2.5,1,0), las = 1)
  
  axis(side = 1, at = seq(from = 0, to = 8, by = 2), labels = c(0,2,4,6,8))
  axis(side = 2, at = seq(from = 0.95, to = 1.10, by = 0.05), labels = format(c(0.950, 1.000, 1.050, 1.100), nsmall = 3))
  
  #Add horizontal line at ratio = 1
  abline(h = 1, lwd = 1)
  
  #Add text to explain the horizontal line
  text(x = (0.0025*par("usr")[1] + 0.9975*par("usr")[2]), y = (1 - 0.015*(par("usr")[4] - par("usr")[3])), "Ratio = 1.000", cex = 0.8, adj = 1)
  
  #add titles
  title(main = paste("[KH-008S] Pyranometer/Silicon Ratio vs. Daily GHI (Silicon)"), line = 2.2, cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[114,1], " to ", usefulDataMatrix[dim(usefulDataMatrix)[1],1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("G06-Pyr-Si-vs-Si-KH-008S","\n","Dhruv Baid, ",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotPyrSiVsSi()

#Plot Sat/Pyr Ratio against GHI (Pyr)
plotSatPyrVsPyr = function() {
  #plot points for pyranometer and silicon
  plot(usefulDataMatrix[115:dim(usefulDataMatrix)[1],2],usefulDataMatrix[115:dim(usefulDataMatrix)[1],15], xlab = expression('Daily Global Horizontal Irradiation - SMP10) [kWh/m'^2*']'), ylab = "GSI/Pyr Ratio", xaxt = "n", yaxt = "n", xlim = c(0,8), col = "red", pch = 16, cex.lab = 1.3, mgp = c(2.5,1,0), las = 1)
  
  axis(side = 1, at = seq(from = 0, to = 8, by = 2), labels = c(0,2,4,6,8))
  axis(side = 2, at = seq(from = round(par("usr")[3], digits = 1), to = round(par("usr")[4], digits = 1), by = 0.1), labels = format(seq(from = round(par("usr")[3], digits = 1), to = round(par("usr")[4], digits = 1), by = 0.1), nsmall = 3), las = 2, mgp = c(3,0.5,0))
  
  #Add horizontal line at ratio = 1
  abline(h = 1, lwd = 1)
  
  #Add text to explain the horizontal line
  text(x = (0.0025*par("usr")[1] + 0.9975*par("usr")[2]), y = (1 - 0.015*(par("usr")[4] - par("usr")[3])), "Ratio = 1.000", cex = 0.8, adj = 1)
  
  #add titles
  title(main = paste("[KH-008S] Satellite/Pyranometer Ratio vs. Daily GHI (Pyranometer)"), line = 2.2, cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[115,1], " to ", usefulDataMatrix[dim(usefulDataMatrix)[1],1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("G07-Sat-Pyr-vs-Pyr-KH-008S","\n","Dhruv Baid, ",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotSatPyrVsPyr()

#defining a new matrix for water levels
waterLevelMatrix = matrix(nrow = length(dateTimeList) + 1, ncol = 2)
waterLevelMatrix[1,] = c("Date/Time","Water Level")

#populating water levels matrix
waterLevelMatrix[2:nrow(waterLevelMatrix),1] = unlist(dateTimeList)
waterLevelMatrix[2:nrow(waterLevelMatrix),2] = unlist(waterLevelList)

#save .txt file to the right directory
setwd("/home/admin/Jason/cec intern/results/KH008S")
write.matrix(waterLevelMatrix, "timedWaterLevels.txt", sep = "\t")

#Plot Water Levels against time
plotWaterLevels = function() {
  plot(unlist(lapply(waterLevelMatrix[166467:dim(waterLevelMatrix)[1],1], as.POSIXct)), unlist(lapply(waterLevelMatrix[166467:dim(waterLevelMatrix)[1],2],as.numeric)), type = "l", ylim = c(2,10), xaxt = "n", yaxt = "n", xlab = "", ylab = "Water Levels", cex.lab = 1.3, col = "steelblue1", lwd = 1)
  
  #index of dates in mmm/YY format
  index_y = unlist(lapply(waterLevelMatrix[166467:dim(waterLevelMatrix)[1],1], as.POSIXct))
  datesIndex_y = unlist(lapply(lapply(waterLevelMatrix[166467:dim(waterLevelMatrix)[1],1],as.Date), format, "%b/%y"))
  
  #actual labels for date-axis (i.e. non-duplicated ones)
  dateLabels = datesIndex_y[!duplicated(datesIndex_y)]
  
  #list of points on the default x-axis where the new labels should go (i.e. points in the list of dates which were not duplicates - start of year)
  at_tick = index_y[which(!duplicated(datesIndex_y))]
  
  #labelling time-axis
  axis(side = 1, at = at_tick, labels = dateLabels, las = 2, cex.axis = 0.5)
  
  #label y-axis
  axis(side = 2, at = seq(from = 2, to = 10, by = 1), labels = format(seq(from = 2, to = 10, by = 1), nsmall = 2), las = 1, mgp = c(3,0.7,0))
  
  #add line for flooding level
  abline(h = 9.66)
  
  #add text to explain this line
  text(x = (0.99*par("usr")[1] + 0.01*par("usr")[2]), y = 9.66 - 0.02*(par("usr")[4] - par("usr")[3]), "Reservoir Flooding Level", adj = 0)
  
  #add titles
  title(main = paste("[KH-008S] Water Levels"), line = 2.2, cex.main = 1.5)
  title(main = paste0("From ",as.Date(waterLevelMatrix[166467,1]), " to ", as.Date(waterLevelMatrix[dim(waterLevelMatrix)[1],1])), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("G08-WtrLvl-KH-008S","\n","Dhruv Baid, ",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotWaterLevels()
dev.off()
