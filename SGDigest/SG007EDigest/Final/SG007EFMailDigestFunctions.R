rm(list = ls())

USEPAVG = 0
MEPAVG = 0
RATIOAVG = 0
path4G = "/home/admin/Dropbox/Fourth_Gen/[SG-007E]/Final-Settlements/[SG-007E-F]-lifetime.txt"
RESET4G = 1

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

secondGenData = function(filepath,writefilepath)
{
	temp = unlist(strsplit(filepath,"\\/"))
	fileName = temp[length(temp)]
  temp = unlist((strsplit(temp[length(temp)]," ")))
	temp = unlist((strsplit(temp[length(temp)],"\\.")))
	
	dataread = read.table(filepath,header=T,sep = "\t")
	
	c0 = temp[1]

	coldata = as.numeric(dataread[,2])
	c1 = sum(coldata[complete.cases(coldata)]*1000)

	coldata = as.numeric(dataread[,3])
	c2  = round(mean(coldata[complete.cases(coldata)]),2)

	coldata = as.numeric(dataread[,4])
	c3 = sum(coldata[complete.cases(coldata)])

	coldata = as.numeric(dataread[,5])
	c4 = sum(coldata[complete.cases(coldata)]*1000)

	coldata = as.numeric(dataread[,6])
	c5 = round(mean(coldata[complete.cases(coldata)]),2)

	coldata = as.numeric(dataread[,7])
	c6 = sum(coldata[complete.cases(coldata)])

	coldata = as.numeric(dataread[,9])
	c7 = sum(coldata[complete.cases(coldata)])

	c8 = round(abs(((c5/c2)*100)-100),2)

	c9 = round(abs(((c4/c1)*100)-100),2)

	c10 = as.numeric(c1) - as.numeric(c4)
	
	df = data.frame(Date = c0, IEQTot=c1, MEPAvg=c2, GenSCNTot=c3, WEQTot=c4, USEPAvg=c5,
									LESDPTot=c6, NETTot=c7, USEPMEPRat=c8, ExportPerc=c9,ExportVol = c10)
	colnames(df) = c("Date","IEQTot (kWh)","MEPAvg ($c/kWh)","GESCNTot ($)","WEQTot (kWh)",
									"USEPAvg ($c/kWh))","LESDPTot ($)","NETTot ($)","USEPMEPRat","ExportPerc","Export Volume (kWh)")
	
	write.table(df,file=writefilepath,row.names=F,col.names=T,sep="\t",append=F)
}

thirdGenData = function(filepathm1,writefilepath)
{
  df =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F)
  day = as.character(df[1,1])
	idxExist = NA
  {
    if(file.exists(writefilepath))
    {
			df3G = read.table(writefilepath,header = T,sep="\t",stringsAsFactors=F)
			daysAll = as.character(df3G[,1])
			idxExist = match(day,daysAll)
			if(!is.finite(idxExist))
      	write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
	USEPAVG <<- round(mean(as.numeric(df[,6])),2)
	MEPAVG <<- round(mean(as.numeric(df[,3])),2)
	RATIOAVG <<- round(mean(as.numeric(df[,9])),2)
	if(!is.finite(idxExist))
		fourthGenData(filepathm1)
}

fourthGenData = function(path)
{
	data = read.table(path,header=T,sep="\t",stringsAsFactors=F)
	{
		if(RESET4G)
		{
			write.table(data,file=path4G,sep="\t",row.names=F,col.names=T,append=F)
			RESET4G <<- 0
		}
		else
			write.table(data,file=path4G,sep="\t",row.names=F,col.names=F,append=T)
	}
}
