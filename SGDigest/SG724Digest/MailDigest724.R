errHandle = file('/home/admin/Logs/Logs724Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
system('rm -R "/home/admin/Dropbox/Second Gen/[SG-724S]"')
source('/home/admin/CODE/SGDigest/SG724Digest/runHistory724.R')
require('mailR')
print('History done')
source('/home/admin/CODE/SGDigest/SG724Digest/3rdGenData724.R')
print('3rd gen data called')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/SGDigest/SG724Digest/aggregateInfo.R')
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

preparebody = function(path)
{
  print("Enter Body Func")
	body = ""
	body = paste(body,"Meteorological Station Name: SG-724 Singapore (Fab 2)\n",sep="")
	body = paste(body,"\n Location: GlobalFoundries, Woodlands, Singapore\n")
	body = paste(body,"\n Coordinates: 1.435199, 103.767936\n")     #line added
	body = paste(body,"\n O&M Code: GND-WOOD \n")
	body = paste(body,"\n Monitoring System Code: SG-724\n")
	body = paste(body,"\n Yearly Long-Term GHI: 1,617 kWh/m2 (source: SolarGIS)\n")
	body = paste(body,"\n Monthly Long-Term GHI (daily averages in brackets)\n")
	body = paste(body,"\n\tJan: 129.6 kWh/m2 (3.05)\n")
	body = paste(body,"\n\tFeb: 135.0 kWh/m2 (4.22)\n")
	body = paste(body,"\n\tMar: 153.2 kWh/m2 (5.54)\n")
	body = paste(body,"\n\tApr: 140.5 kWh/m2 (6.28)\n")
	body = paste(body,"\n\tMay: 137.0 kWh/m2 (6.19)\n")
	body = paste(body,"\n\tJun: 128.5 kWh/m2 (5.73)\n")
	body = paste(body,"\n\tJul: 132.3 kWh/m2 (4.87)\n")
	body = paste(body,"\n\tAug: 135.8 kWh/m2 (4.93)\n")
	body = paste(body,"\n\tSep: 142.3 kWh/m2 (4.97)\n")
	body = paste(body,"\n\tOct: 139.0 kWh/m2 (4.66)\n")
	body = paste(body,"\n\tNov: 124.9 kWh/m2 (3.57)\n")
	body = paste(body,"\n\tDec: 119.2 kWh/m2 (3.06)\n")
	body = paste(body,"\n Station First Day of Operations: 2018-07-04\n")
	body = paste(body,"\n System Age [days]:",DAYSACTIVE,"\n")
	body = paste(body,"\n System Age [years]:",rf1(DAYSACTIVE/365))

	for(x in 1 : length(path))
  {
		print(path[x])
		body = paste(body,"\n\n_____________________________________________\n")
		days = unlist(strsplit(path[x],"/"))
		days = substr(days[length(days)],11,20)
		body = paste(body,days)
		body  = paste(body,"\n_____________________________________________\n")
		dataread = read.table(path[x],header = T,sep="\t",stringsAsFactors=F)
		print("===================")
		print(path[x])
		print(ncol(dataread))
		body = paste(body,"\nPts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
		body = paste(body,"\n\nDaily Irradiation Pyranometer [kWh/m2]:",as.character(dataread[1,5]))
		body = paste(body,"\n\nDaily Irradiation GSi00 [kWh/m2]:",as.character(dataread[1,3]))
		body = paste(body,"\n\nDaily Irradiation Gmod [kWh/m2]:",as.character(dataread[1,4]))
		body = paste(body,"\n\nMean Ambient Temp [C]:",as.character(dataread[1,6]))
		body = paste(body,"\n\nMean Ambient Temp Solar Hours [C]:",as.character(dataread[1,7]))
		body = paste(body,"\n\nMax Ambient Temp [C]:",as.character(dataread[1,8]))
		body = paste(body,"\n\nMax Ambient Temp Solar Hours [C]:",as.character(dataread[1,10]))
		body = paste(body,"\n\nMin Ambient Temp [C]:",as.character(dataread[1,9]))
		body = paste(body,"\n\nMin Ambient Temp Solar Hours [C]:",as.character(dataread[1,11]))
		body = paste(body,"\n\nMean Tmod [C]:",as.character(dataread[1,18]))
		body = paste(body,"\n\nMean Tmod Solar Hours [C]:",as.character(dataread[1,23]))
		body = paste(body,"\n\nMax Tmod [C]:",as.character(dataread[1,24]))
		body = paste(body,"\n\nMax Tmod Solar Hours [C]:",as.character(dataread[1,25]))
		body = paste(body,"\n\nMin Tmod [C]:",as.character(dataread[1,26]))
		body = paste(body,"\n\nMin Tmod Solar Hours [C]:",as.character(dataread[1,25]))
		body = paste(body,"\n\nDifference in mean Tmod and Tamb for Solar Hours [C]:",format((as.numeric(dataread[1,23])-as.numeric(dataread[1,7])),nsmall=1))
		body = paste(body,"\n\nMean Humidity [%]:",as.character(dataread[1,12]))
		body = paste(body,"\n\nMean Humidity Solar Hours [%]:",as.character(dataread[1,13]))
		body = paste(body,"\n\nMax Humidity [%]:",as.character(dataread[1,14]))
		body = paste(body,"\n\nMin Humidity [%]:",as.character(dataread[1,15]))
		body = paste(body,"\n\nMax Humidity Solar Hours [%]:",as.character(dataread[1,16]))
		body = paste(body,"\n\nMin Humidity Solar Hours [%]:",as.character(dataread[1,17]))
		body = paste(body,"\n\nGmod/GSi Ratio:",as.character(dataread[1,19]))
		body = paste(body,"\n\nPyr/Si Ratio:",as.character(dataread[1,20]))
		body = paste(body,"\n\nAverage Wind Direction:",as.character(dataread[1,21]))
		body = paste(body,"\n\nAverage Wind Direction Solar Hours:",as.character(dataread[1,22]))
		body = paste(body,"\n\nStdev Wind Direction [m/s]:",as.character(format(round(dataread[1,33]))))
		body = paste(body,"\n\nCOV Wind Direction [%]:",as.character(format(round(dataread[1,34]))))
		body = paste(body,"\n\nAverage Wind Speed [m/s]:",as.character(dataread[1,28]))
		body = paste(body,"\n\nMax Wind Speed [m/s]:",as.character(dataread[1,29]))
		body = paste(body,"\n\nAverage Wind Speed Solar hours [m/s]:",as.character(dataread[1,30]))
		body = paste(body,"\n\nStdev Wind Speed [m/s]:",as.character(dataread[1,31]))
		body = paste(body,"\n\nCOV Wind Speed [%]:",as.character(dataread[1,32]))
		print(as.character(dataread[1,31]))
		print(as.character(dataread[1,32]))
		print(as.character(dataread[1,33]))
		print(as.character(dataread[1,34]))
		print("==============")
  }
print('Daily Summary for Body Done')
		body = paste(body,"\n\n_____________________________________________\n")

  body = paste(body,"\nStation History\n")
		body = paste(body,"\n_____________________________________________\n\n")

	body = paste(body,"Station Birth Date:",dobstation)
	body = paste(body,"\n\n# Days station active:",DAYSACTIVE)
	body = paste(body,"\n\n# Years station active:",rf1(DAYSACTIVE/365))
	body = paste(body,"\n\nTotal irradiation from uncleaned Si sensor this month [kWh/m2]:",MONTHLYIRR1)
	body = paste(body,"\n\nTotal irradiation from cleaned Si sensor this month [kWh/m2]:",MONTHLYIRR2)
	body = paste(body,"\n\nTotal irradiation  measured by Pyranometer [kWh/m2]:",MONTHLYIRR3)
	 body = paste(body,"\n\nAverage irradiation from uncleaned Si sensor this month [kWh/m2]:",MONTHAVG1)
	   body = paste(body,"\n\nAverage irradiation from cleaned Si sensor this month [kWh/m2]:",MONTHAVG2)
		   body = paste(body,"\n\nAverage irradiation measured by Pyranometer this month [kWh/m2]:",MONTHAVG3)
	body = paste(body,"\n\nForecasted irradiation total for uncleaned Si sensor this month [kWh/m2]:",FORECASTIRR1);
	body = paste(body,"\n\nForecasted irradiance total for cleaned Si sensor this month [kWh/m2]:",FORECASTIRR2);
	body = paste(body,"\n\nForecasted irradiance total for Pyranamoter this month [kWh/m2]:",FORECASTIRR3)
	body = paste(body,"\n\nIrradiation total last 30 days from cleaned Si sensor [kWh/m2]:",LAST30DAYSTOT)
  body = paste(body,"\n\nIrradiation total last 30 days from Pyranometer [kWh/m2]:",LAST30DAYSTOT2)
	body = paste(body,"\n\nIrradiation average last 30 days from cleaned Si sensor [kWh/m2]:",LAST30DAYSMEAN)
	body = paste(body,"\n\nIrradiance avg last 30 days from Pyranometer[kWh/m2]:",LAST30DAYSMEAN2)
	body = paste(body,"\n\nSoiling loss (%):",SOILINGDEC)
	body = paste(body,"\n\nSoiling loss per day (%)",SOILINGDECPD)
	body = paste(body,"\n\nPyr/Si loss (%):",SNPDEC)
	body = paste(body,"\n\nPyr/Si loss per day (%)",SNPDECPD)
print('3G Data Done')	
return(body)

}
path = "/home/admin/Dropbox/Cleantechsolar/1min/[724]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-724S]"
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[SG-724S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
#if(length(dir(pathmonth)) > 1)
#{
#  prevday = dir(pathmonth[length(dir(pathmonth))-1])
#}

stnnickName = "724"
stnnickName2 = "SG-724S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
	yr = substr(lastdatemail,1,4)
	monyr = substr(lastdatemail,1,7)
	prevpathyear = paste(path,yr,sep="/")
	prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
	prevpathmonth = paste(prevpathyear,monyr,sep="/")
	currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
	prevwriteyears = paste(pathwrite,yr,sep="/")
	prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}

check = 0
sender <- "operations@cleantechsolar.com" 
uname <- "shravan.karthik@cleantechsolar.com"
pwd = 'CTS&*(789'
recipients = getRecipients("SG-724S","m")
wt =1
idxdiff = 1
while(1)
{
recipients = getRecipients("SG-724S","m")
recordTimeMaster("SG-724S","Bot")
years = dir(path)
writeyears = paste(pathwrite,years[length(years)],sep="/")
checkdir(writeyears)
pathyear = paste(path,years[length(years)],sep="/")
months = dir(pathyear)
writemonths = paste(writeyears,months[length(months)],sep="/")
sumname = paste("[SG-724S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
checkdir(writemonths)
pathmonth = paste(pathyear,months[length(months)],sep="/")
if(length(dir(pathmonth)) < 1)
{
	print('No file in the folder yet..')
	Sys.sleep(3600)
	next
}
tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
currday = dir(pathmonth)[length(dir(pathmonth))]
if(prevday == currday)
{
   if(wt == 1)
{
 print("")
 print("")
 print("__________________________________________________________")
 print(substr(currday,7,16))
 print("__________________________________________________________")
 print("")
 print("")
 print('Waiting')
wt = 0
}
{
  if(tm > 1319 || tm < 30)
  {
      Sys.sleep(120)
  }        
  else
  {
    Sys.sleep(3600)
  }
}
next
}
print('New Data found.. sleeping to ensure sync complete')
Sys.sleep(20)
print('Sleep Done')
idxdiff = match(currday,dir(pathmonth)) - match(prevday,dir(prevpathmonth))
if(idxdiff < 0)
{
	idxdiff = length(dir(prevpathmonth)) - match(prevday,dir(prevpathmonth)) + length(dir(pathmonth))
	newmonth = pathmonth
	newwritemonths = writemonths
	pathmonth = prevpathmonth
	writemonths = prevwritemonths
}
while(idxdiff)
{
{
if(prevday == dir(prevpathmonth)[length(dir(prevpathmonth))])
{
  pathmonth = newmonth
	writemonths = newwritemonths
  currday = dir(pathmonth)[1]
  monthlyirr1 = monthlyirr2 = monthlyirr3 = 0
  strng = unlist(strsplit(months[length(months)],"-"))
  daystruemonth = nodays(strng[1],strng[2])
  dayssofarmonth = 0
}	
else
currday = dir(pathmonth)[(match(prevday,dir(pathmonth)) +1)]
}
dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t")
datawrite = prepareSumm(dataread)
datasum = rewriteSumm(datawrite)
currdayw = gsub("724","SG-724S",currday)
write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)



monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3])
monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4])
monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5])
globirr1 = globirr1 + as.numeric(datawrite[1,3])
globirr2 = globirr2 + as.numeric(datawrite[1,4])
globirr3 = globirr3 + as.numeric(datawrite[1,5])
dayssofarmonth = dayssofarmonth + 1    
daysactive = daysactive + 1
last30days[[last30daysidx]] = as.numeric(datawrite[1,4])
last30days2[[last30daysidx]] = as.numeric(datawrite[1,5])



print('Digest Written');
{
  if(!file.exists(paste(writemonths,sumname,sep="/")))
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    print('Summary file created')
}
  else 
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
   print('Summary file updated')  
}
}
summary_path <- paste('/home/admin/Jason/cec intern/results/[724]/WindRose.pdf', sep="")
filetosendpath = c(paste(writemonths,currdayw,sep="/"))
filestosendpath = c(filetosendpath,summary_path)
filename = c(currday)
filedesc = c("Daily Digest")
dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t")
datawrite = prepareSumm(dataread)
prevdayw = gsub("724","SG-724S",prevday)

dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t")
print('Summary read')
if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
{
print('Difference in old file noted')



previdx = (last30daysidx - 1) %% 31

if(previdx == 0) {previdx = 1}
  datasum = rewriteSumm(datawrite)
prevdayw = gsub("724","SG-724S",prevday)

  write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)


if(prevpathmonth == pathmonth)
{
	monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
  monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
	monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
}
globirr1 = globirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
globirr2 = globirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
globirr3 = globirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
last30days[[previdx]] = as.numeric(datawrite[1,4])
last30days2[[previdx]] = as.numeric(datawrite[1,5])



datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t")
  rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
  print(paste('Match found at row no',rn))
  datarw[rn,] = datasum[1,]
  write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
  filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
  filename[2] = prevday
  filedesc[2] = c("Updated Digest")
print('Updated old files.. both digest and summary file')
}

last30daysidx = (last30daysidx + 1 ) %% 31
if(last30daysidx == 0) {last30daysidx = 1}

LAST30DAYSTOT = rf(sum(last30days))
LAST30DAYSMEAN = rf(mean(last30days))
LAST30DAYSTOT2 = rf(sum(last30days2))
LAST30DAYSMEAN2 = rf(mean(last30days2))
DAYSACTIVE = daysactive
MONTHLYIRR1 = monthlyirr1
MONTHLYIRR2 = monthlyirr2
MONTHLYIRR3 = monthlyirr3
MONTHAVG1 = rf(monthlyirr1 / dayssofarmonth)
MONTHAVG2 = rf(monthlyirr2 / dayssofarmonth)
MONTHAVG3 = rf(monthlyirr3 / dayssofarmonth)
FORECASTIRR1 = rf(as.numeric(MONTHAVG1) * daystruemonth)
FORECASTIRR2 = rf(as.numeric(MONTHAVG2) * daystruemonth)
FORECASTIRR3 = rf(as.numeric(MONTHAVG3) * daystruemonth)
SOILINGDEC = rf3(((globirr1 / globirr2) - 1) * 100)
SNPDEC = rf3(((globirr3 / globirr2) - 1) * 100)
SOILINGDECPD = rf3(as.numeric(SOILINGDEC) / DAYSACTIVE)
SNPDECPD = rf3(as.numeric(SNPDEC) / DAYSACTIVE)

print('Sending mail')
body = ""
body = gsub("\n ","\n",body)
body = preparebody(filetosendpath)

send.mail(from = sender,
          to = recipients,
          subject = paste("Station [SG-724S] Digest",substr(currday,7,16)),
          body = body,
          smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
          authenticate = TRUE,
          send = TRUE,
          attach.files = filestosendpath,
          #file.names = filename, # optional parameter
          #file.descriptions = filedesc, # optional parameter
          debug = F)
print('Mail Sent')
recordTimeMaster("SG-724S","Mail",substr(currday,7,16))
prevday = currday
prevpathyear = pathyear
prevpathmonth = pathmonth
prevwriteyears = writeyears
prevwritemonths = writemonths
prevsumname = sumname
idxdiff = idxdiff - 1;
wt = 1
}
gc()
}
sink()
