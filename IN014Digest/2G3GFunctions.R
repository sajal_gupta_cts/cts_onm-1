constYieldVal = c(226.800,378.00,453.600)
timetomins = function(x)
{
	lists = unlist(strsplit(x,"\\ "))
	seq1 = seq(from = 2, to = length(lists),by=2)
	lists = lists[seq1]
	lists = unlist(strsplit(lists,":"))
	seq1 = seq(from = 1, to = length(lists),by = 2)
	seq2 = seq(from = 2, to = length(lists),by = 2)
	hr = as.numeric(lists[seq1])
	min = as.numeric(lists[seq2])
	return((hr * 60)+min+1)
}

getPRData = function(df)
{
	gsi=PR1=PR2=NA
	date = as.character(df[,1])
	yr = substr(date,1,4)
	mon=substr(date,1,7)
	date = substr(date,1,10)
	path = paste('/home/admin/Dropbox/Second Gen/[IN-015S]/',yr,"/",mon,"/[IN-015S] ",date,".txt",sep="")
	print(paste('Path reading GSI is from',path))
	if(file.exists(path))
	{
	print('File exists')
	dataread = read.table(path,header = T,sep = "\t",stringsAsFactors=F)
	gsi = as.numeric(dataread[1,3])
	PR1 = round(as.numeric(df[,5])*100/gsi,1)
	PR2 = round(as.numeric(df[,6])*100/gsi,1)
	}
	array2 = c(gsi,PR1,PR2)
	return(array2)
}

secondGenData = function(path,pathw,mtno)
{
	dataread = read.table(path,header = T,sep = "\t")
	irr = 0.00
	{
	if(nrow(dataread) < 1)
	{
		day = unlist(strsplit(path,"/"))
		day = day[length(day)]
		day = unlist(strsplit(day,"\\ "))
		day = substr(day[2],1,10)
		yr = as.numeric(substr(day,1,4))
		mon = as.numeric(substr(day,6,7))
	}
	else
	{
	day = as.character(dataread[1,1])
	day = unlist(strsplit(day,"\\ "))
	day = day[1]
		yr = as.numeric(substr(day,1,4))
		mon = as.numeric(substr(day,6,7))
	}
	}
	multiplyFactor = 2
	if(((yr < 2017) || (yr ==2017 && mon<7)))
		multiplyFactor = 1
	DA = format(round(nrow(dataread)*multiplyFactor/2.88,1),nsmall=1)
	eac = as.numeric(dataread[,3])
	tmstmpo = as.character(dataread[complete.cases(eac),1])
	{
		if(length(tmstmpo) > 1)
		{
			tmstmpo = tmstmpo[length(tmstmpo)]
		}
		else
		{
			{
				if(nrow(dataread) > 1)
				{
					tmstmpo = as.character(dataread[nrow(dataread),1])
				}
				else 
				{
					tmstmpo = "NA"
				}
			}
		}
	}
	eac = eac[complete.cases(eac)]
	{
	if(length(eac) > 1)
	{
	eaclast = format(round(eac[length(eac)],1),nsmall=1)
	stp1 = eac[length(eac)] - eac[1]
	eac = format(round(stp1,1),nsmall=1)
	}
	else
	{
	eac = "NA"
	eaclast = "NA"
	}
	}
	pac = as.numeric(dataread[,2])
	pac = pac[complete.cases(pac)]
	{
	if(length(pac)>1){
	pac = format(round(sum(pac)*multiplyFactor/12000,1),nsmall=1)}
	else
		pac="NA"
	}
	y1 = format(round(as.numeric(pac)/constYieldVal[mtno],2),nsmall=2)
	y2 = format(round(as.numeric(eac)/constYieldVal[mtno],2),nsmall = 2)
	df = data.frame(Date = day,DA = DA,EacM1=pac,EacM2=eac,Yield1=y1,Yield2=y2,EacLast=eaclast,TmLast=tmstmpo,stringsAsFactors=F)
	vals=getPRData(df)
	df = data.frame(Date = day,DA = DA,EacM1=pac,EacM2=eac,Yield1=y1,Yield2=y2,EacLast=eaclast,TmLast=tmstmpo,GSI=vals[1],PR1=vals[2],PR2=vals[3],stringsAsFactors=F)
	write.table(df,file = pathw,row.names = F,col.names = T,sep = "\t",append = F)
}

thirdGenData = function(pathr,pathw)
{
	pathr = read.table(pathr,header = T,sep = "\t")
	if(!file.exists(pathw))
	{
		write.table(pathr,file = pathw,row.names = F,col.names = T,sep = "\t",append = F)
		return()
	}
	write.table(pathr,file = pathw,row.names = F,col.names = F,sep = "\t",append = T)
}
