from flask import Flask, render_template, request
import requests
import pandas as pd
import itertools
from numpy.random import randint
import datetime
import re
import ast
import werkzeug._internal

def demi_logger(type, message,*args,**kwargs):
    pass

path='/home/admin/Dropbox/Lifetime/Gen-1/'
recipientspath='/home/admin/CODE/Send_mail/'
lifetimepath='/home/pranav/Lifetime3.csv'
app = Flask(__name__)
dflifetime=pd.read_csv(lifetimepath)
cols=['Seris_Station_Name','Flexi_Station_Name','Locus_Station_Name','Meter_Cols2','Meter_Cols_Flexi','Locus_Meter_Cols','Seris_No_Meters','Flexi_No_Meters','Locus_No_Meters']
stnnames=[]
metercols=[]

for i in range(3):
    meterno=[]
    temp=dflifetime[cols[i]].dropna().tolist()
    for k in temp:
        temp2=k[1:7]
        stnnames.append(temp2)
    temp=list(map(int,dflifetime[cols[i+6]].dropna().tolist()))
    for l in temp:
        meterno.append(l)
    temp=dflifetime[cols[i+3]].dropna().tolist()
    for m,n in enumerate(temp):
        temp=ast.literal_eval(n)
        temp1=temp2=temp3=''
        if(i==0):
            for o in range(2*meterno[m]):
                temp2=temp[o+2]+';'
                temp3=temp3+temp2
            metercols.append(temp3)
        elif(i==2):
            for o in range(2*meterno[m]):
                temp2=temp[o+2]+';'
                temp3=temp3+temp2
            temp_arr=temp3.split(';')
            for index,t in enumerate(temp_arr):
                if 'LastRead' in t:
                    temp_arr[index]='LastR-'+temp_arr[index][:-9]
                elif('Eac' in t):
                    temp_arr[index]='Eac-'+temp_arr[index][:-5]
            temp3=(';').join(temp_arr)
            metercols.append(temp3)
        else:
            for o in range(meterno[m]):
                str1='LastR-'+temp[o]+';'
                str2='Eac-'+temp[o]+';'
                temp1=temp1+str1
                temp2=temp2+str2
            metercols.append(temp1+temp2)
metercols = [i[ : -1] for i in metercols] 
cols=dict(zip(stnnames, metercols))  
hardstn=['SG-006']
hardfinal=['LastR-Admin;LastR-Canteen;LastR-EM;LastR-Mill;LastR-Warehouse;Eac-Admin;Eac-Canteen;Eac-EM;Eac-Mill;Eac-Warehouse']
stnnames=stnnames+hardstn
metercols=metercols+hardfinal
for i,j in enumerate(stnnames):
    if j=='own':
        del stnnames[i]
        del metercols[i]
cols=dict(zip(stnnames, metercols)) 

@app.route('/')
def index():
    return render_template('home.html',stnnames=sorted(list(stnnames)))

@app.route('/sms_page')
def sms_page():
    return render_template('sms_page.html')

@app.route('/modify/<Type>/<name>',methods=['GET', 'POST'])
def modify(Type,name):
    if request.method == 'POST':
        data = request.form.to_dict()
        key=data['SecretKey']
        del data['SecretKey']
        if(key!='Cleantech1@'):
            return render_template('sms_page.html',val=500),500
        else:
            if(Type=='sms'):
                df=pd.read_csv(recipientspath+'alerts_recipients.csv')
                names=df['Name'].tolist()
            elif(Type=='mail'):
                df=pd.read_csv(recipientspath+'mail_recipients.csv')
                names=df['Recipients'].tolist()
            vals=[]
            for i in names:
                if i in data.keys():
                    vals.append(1)
                else:
                    vals.append(0) 
            df[name]=vals
            if(Type=='sms'):
                df.to_csv(recipientspath+'alerts_recipients.csv',mode='w',index=False)
                return render_template('sms_page.html',val=200),200
            elif(Type=='mail'):
                df.to_csv(recipientspath+'mail_recipients.csv',mode='w',index=False)
                return "SUCCESS!"
    else:
        if(Type=='sms'):
            df=pd.read_csv(recipientspath+'alerts_recipients.csv')
            names=df['Name'].tolist()
        elif(Type=='mail'):
            df=pd.read_csv(recipientspath+'mail_recipients.csv')
            names=df['Recipients'].tolist()
        vals=df[name].tolist()
        final=zip(names,vals)
        print(final)
        return render_template('recipients_home.html',final=final,station=name,Type=Type)
    

@app.route('/somepage',methods=['POST'])
def renderstationreplacepage():
    data = request.form.to_dict()
    station=data['Station']
    print(data,station)
    if data['action'] == 'ADD NEW DATA':
        cols2=cols[station]
        cols2=cols2.split(";")
        print(cols2)
        return render_template('addpage.html',val=cols2,stationname=station)
    elif data['action'] == 'REPLACE DATA':  
        cols2=cols[station]
        cols2=cols2.split(";")
        print(cols2)
        return render_template('replacepage.html',val=cols2,stationname=station)

@app.route('/addnewdata',methods=['POST'])
def addnewdata():
    data = request.form.to_dict()
    print(data)
    key=data['SecretKey']
    del data['SecretKey']
    station=data['Station']
    del data['Station']
    date=data['Date']
    df=pd.read_csv(path+station+'-LT.txt',sep='\t')
    if((df['Date']==date).any()):
        return render_template('home.html', val=409,stnnames=sorted(list(stnnames))),409
    ogcols=df.columns
    print(ogcols)
    vals=[]
    for i in ogcols:
        if i[0:5]=='LastT':
            vals.append( str(data['Date'])+ ' 23:59:00')
        else:
            vals.append(data[i])
    print(vals)
    df.loc[len(df)] = vals
    if(key=='Cleantech1@'):
        df.to_csv(path+station+'-LT.txt',sep='\t',mode='w',index=False)
        return render_template('home.html', val=200,stnnames=sorted(list(stnnames))),200
    else:
        return render_template('home.html', val=500,stnnames=sorted(list(stnnames))),500

@app.route('/replacedata',methods=['POST'])
def replacedata():
    data = request.form.to_dict()
    print(data)
    key=data['SecretKey']
    del data['SecretKey']
    station=data['Station']
    del data['Station']
    valtype=data['Type']
    del data['Type']
    date=data['Date']
    prevdate=datetime.datetime.strptime(date, "%Y-%m-%d")+datetime.timedelta(-1)
    print(prevdate)
    del data['Date']    
    vals=list(data.values())
    keys=list(data.keys())
    df=pd.read_csv(path+station+'-LT.txt',sep='\t')
    if(valtype[0:5]=='LastR'):
        tempkey='Eac'+valtype[5:]
        prevval=df.loc[df['Date']==str(prevdate.date()),valtype]
        if(prevval.empty):
            return render_template('home.html', val=502,stnnames=sorted(list(stnnames))),502
        keys.append(tempkey)
        vals.append(float(data[valtype])-float(prevval))
    elif(valtype[0:3]=='Eac'):
        tempkey='LastR'+valtype[3:]
        prevval=df.loc[df['Date']==str(prevdate.date()),tempkey]
        if(prevval.empty):
            return render_template('home.html', val=502,stnnames=sorted(list(stnnames))),502
        keys.append(tempkey)
        vals.append(float(data[valtype])+float(prevval))
    df.loc[df['Date']==str(date),keys]=vals
    if(key=='Cleantech1@' and not df.loc[df['Date']==str(date),keys].empty):
        df.to_csv(path+station+'-LT.txt',sep='\t',mode='w',index=False,header=True)
        return render_template('home.html', val=200,stnnames=sorted(list(stnnames))),200
    elif(df.loc[df['Date']==str(date),keys].empty):
        return render_template('home.html', val=501,stnnames=sorted(list(stnnames))),501
    else:
        return render_template('home.html', val=500,stnnames=sorted(list(stnnames))),500

if __name__ == '__main__':
    werkzeug._internal._log = demi_logger
    app.run(host = '0.0.0.0',port=5005,debug=True)