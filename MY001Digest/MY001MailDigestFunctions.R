rm(list = ls())
TIMESTAMPSALARM = NULL
ltcutoff = .001
CONSTYIELD=313.5
path4G = "/home/admin/Dropbox/Fourth_Gen/[MY-001X]/[MY-001X]-lifetime.txt"
RESET4G = 1
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

fetchGSIData = function(date)
{
	pathMain = '/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[MY-002C]'
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[MY-002C]-WMS-',date,".txt",sep="")
	pathyr = paste(pathMain,yr,sep="/")
	gsiVal = NA
	if(file.exists(pathyr))
	{
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon))
		{
			pathfile = paste(pathmon,"WMS",txtFileName,sep="/")
			if(file.exists(pathfile))
			{
				dataread = read.table(pathfile,sep="\t",header = T)
				gsiVal = as.numeric(dataread[,3])
			}
		}
	}
	return(gsiVal)
}

secondGenData = function(filepath,writefilepath)
{
	TIMESTAMPSALARM <<- NULL
  
	dataread = try(read.table(filepath,header = T,sep = "\t"),silent=F)
	dataread2 = dataread
	lastread = lasttime = Eac2 = "NA"
	if(class(dataread)=="try-error" || ncol(dataread) < 9)
	{
		return(NA)
	}
	dataread = dataread[complete.cases(dataread[,39]),]
	if(nrow(dataread) > 0)
	{
	lastread = as.character(as.numeric(dataread[nrow(dataread),39])/1000)
	lasttime = as.character(dataread[nrow(dataread),1])
	Eac2 = format(round((as.numeric(dataread[nrow(dataread),39]) - as.numeric(dataread[1,39]))/1000,1),nsmall=1)
	}
	dataread = dataread2[complete.cases(dataread2[,15]),]
	dataread = dataread[as.numeric(dataread[,15]) > 0,]
	Eac1 = 0
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,15]))/60000,1),nsmall=1)
	}
	dataread = dataread2[complete.cases(dataread2[,9]),]
	dataread = dataread[as.numeric(dataread[,9]) < 0,]
	
	dataread = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(dataread2[,15]),]
  missingfactor = 600 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,15]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		TIMESTAMPSALARM <<- as.character(dataread2[,1])
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/1.2,1),nsmall=1)
	date = substr(as.character(dataread[1,1]),1,10)
	gsiVal = fetchGSIData(date)
	dspyield1 = round((as.numeric(Eac1)/CONSTYIELD),2)
	dspyield2 = round((as.numeric(Eac2)/CONSTYIELD),2)
	PR1 = round((dspyield1*100/as.numeric(gsiVal)),1)
	PR2 = round((dspyield2*100/as.numeric(gsiVal)),1)
  df = data.frame(Date = date, Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
             #LoadDelivered = as.numeric(Eac22),EnergyRecv=as.numeric(Eac3),
						 DA = DA,Downtime = totrowsmissing,
						LastTime = lasttime, LastReadDel = lastread,
						#LastReadRecv=lastread2,GSI=gsiVal,
						DailySpecYield1=dspyield1,
						DailySpecYield2=dspyield2,
						PR1=PR1,
						PR2=PR2,
						GHIMY002=gsiVal,
						stringsAsFactors = F)
	
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,writefilepath,tmPush)
{
  dataread1 = try(read.table(filepathm1,header = T,sep = "\t",stringsAsFactors=F),silent=T) #its correct dont change
	if(class(dataread1) == "try-error")
		return()
	dateac = as.character(dataread1[1,1])
	if(nchar(dateac) < 10)
	{
		print(paste("Error in date read.. date read was",dateac,"and its incorrect"))
		return()
	}
  {
    if(!file.exists(writefilepath))
    {
      write.table(dataread1,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
    else
    {
  		dataread2 = read.table(writefilepath,header = T,sep = "\t",stringsAsFactors=F)
			dateall = as.character(dataread2[,1])
			idxmtch = match(dateac,dateall)
			print(paste("idxmtch 3G is",idxmtch))
			{
			if(!is.finite(idxmtch))
      	write.table(dataread1,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
			else
			{
				dataread2[idxmtch,] = dataread1[1,]
      	write.table(dataread2,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
			}
			}
    }
	}
	fourthGenData(filepathm1,tmPush)
}

fourthGenData = function(path,tmPush)
{
	data = read.table(path,header=T,sep="\t",stringsAsFactors=F)
	dateac = as.character(data[,1])
	colnamesuse = c(colnames(data),"TimeUpdated","StnID")
	if(is.na(tmPush))
		tmPush = as.character(format(Sys.time(),tz = "Singapore"))
	data[1,(ncol(data)+1)] = tmPush
	data[1,(ncol(data)+1)] = "MY-001X"
	colnames(data) = colnamesuse
	{
		if(RESET4G)
		{
			write.table(data,file=path4G,sep="\t",row.names=F,col.names=T,append=F)
			RESET4G <<- 0
		}
		else
		{
  		dataread2 = read.table(path4G,header = T,sep = "\t",stringsAsFactors=F)
			dateall = as.character(dataread2[,1])
			idxmtch = match(dateac,dateall)
			print(paste(idxmtch,"is idxmtch"))
			{
			if(!is.finite(idxmtch))
      	write.table(data,file = path4G,row.names = F,col.names = F,sep = "\t",append = T)
			else
			{
				dataread2[idxmtch,] = data[1,]
      	write.table(dataread2,file = path4G,row.names = F,col.names = T,sep = "\t",append = F)
			}
			}
		}
	}
}
